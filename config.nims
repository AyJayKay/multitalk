import strformat, strutils
import os except paramStr, paramCount, existsFile
import util-src/path_helper

proc constructOutputPath(parentFolder: string, name: string, outputName: string = ""): (string, string) =
    let outPath = sanitizePathArg(thisDir() / "out" / (if outputName == "": name else: outputName))
    let sourcePath = sanitizePathArg(thisDir() / parentFolder / fmt"{name}.nim")
    return (sourcePath, outPath)

proc taskParamsOnto(expectedLeadingKeys: varargs[string]): seq[string] =
    if paramCount() > 2 and paramStr(2).toLower() in expectedLeadingKeys:
        for i in 3..paramCount():
            result.add(paramStr(i))

proc checkSetupPath(path: string) =
    if not path.existsFile():
        echo "\nError: '" & path.extractFilename() & "' not found. Run 'nim setup' first.\n"
        quit(1)

proc copyCodeProc() =
    let path = thisDir() / "out" / "copy_code"
    checkSetupPath(path)
    let pathArg = sanitizePathArg(path)

    exec pathArg

task setup, "Initializes project: Enables tasks, runs copycode.":
    let execute_tests_paths = constructOutputPath("util-src", "execute_tests")
    exec fmt"nim c -d=release -o={execute_tests_paths[1]} {execute_tests_paths[0]}"

    let copy_code_paths = constructOutputPath("util-src", "copy_code")
    exec fmt"nim c -d=release -o={copy_code_paths[1]} {copy_code_paths[0]}"

    copyCodeProc()

task copycode, "Copies over current code language drafts into sources.":
    copyCodeProc()

const testFilterOptionName = "filter"
const testFilterDescription = "Use 'nim test " & testFilterOptionName & " <partial test names>' for specific tests."
task test, "Compiles and runs tests. " & testFilterDescription:
    let path = thisDir() / "out" / "execute_tests"
    checkSetupPath(path)
    let pathArg = sanitizePathArg(path)

    let partialTestNames = "'" & taskParamsOnto(testFilterOptionName, ":").join(",") & "'"
    if partialTestNames == "''": echo "\n" & testFilterDescription & "\n"

    exec fmt"{pathArg} --{testFilterOptionName}:{partialTestNames}"

const argumentsForwardOptionName = "args"
const argumentsForwardDescription = "Use 'nim dev " & argumentsForwardOptionName & " <arguments>' to forward arguments to Multitalk."
task dev, "Compiles and runs a non release build. " & argumentsForwardDescription:
    let paths = constructOutputPath("src", "main")
    var additionalParams = taskParamsOnto("arg", "args", ":").join(" ")

    exec fmt"nim c --hints=off -o={paths[1]} {paths[0]}"
    # will break here if compiler had exception...
    echo "Dev build successful"
    if additionalParams.len > 0:
        echo fmt"Running Multitalk with args '{additionalParams}':"
    else:
        echo argumentsForwardDescription
        echo "Running Multitalk:"
    echo ""
    exec fmt"{paths[1]} {additionalParams}"

task build, "Compiles release build of Multitalk.":
    copyCodeProc()

    let paths = constructOutputPath("src", "main", "multitalk")
    exec fmt"nim c -d=release --stackTrace=on --lineTrace=on -o={paths[1]} {paths[0]}"