# Welcome to Multitalk!

**About**

This is going to be a lightweight CLI tool for code generated localization, to get rid of magic strings in target source code.
It also manages sheets for your translators (e.g. partial sheets with only updated content) and creates type safe, dynamic loadable content for your application.

The [documentation](https://multitalk.ayjaykay.com) is WIP as well.

No releases yet.

See current progress on [this Trello board](https://trello.com/b/rnqQ7P5C/multitalk).

Find some more info in [this portfolio article](https://ayjaykay.me/#multitalk).

# Example

Check 

1. [src/code-generation/c-sharp/c-sharp-project/example/sheets/english.txt](src/code-generation/c-sharp/c-sharp-project/example/sheets/english.txt)
2. [src/code-generation/c-sharp/c-sharp-project/example/Program.cs](src/code-generation/c-sharp/c-sharp-project/example/Program.cs)

or the [documentation](https://multitalk.ayjaykay.com/#/Workflow).

# Developer

Multitalk is written in [NIM](https://nim-lang.org/)!
A type-safe language, compiling natively [cross-platform](https://nim-lang.org/docs/nimc.html#cross-compilation).

**(Developed from MacOS with NIM 1.0.4)**

## Tasks

The tasks are set up in `config.nims`. ([Nims](https://nim-lang.org/0.20.0/nims.html))

See all custom tasks:

    nim help

**Setup (installation)**

    nim setup

Use this once after clone or whenever you change util code. This will compile helpers (`util-src/` -> `out/`) and initialize the project.

**Copy code**

    nim copycode

This will copy the code language blueprints from example projects into the Multitalk's sources. The config is hardcoded in `util-src/copy_code.nim`.

**Tests**

    nim test

This will compile and run every `.nim` file inside `test/` and sub-folders, (except everything under `test/test-utils/`) and formats the result. You find unit tests and end-to-end tests there.

You can execute specific tests by using `filter` or `:` and the (partial) test names, e.g.:

    nim test : sheet_parser foo bar

**Dev build**

    nim dev

This will compile a non-release build of Multitalk with name `main` into `out/` and executes it, directly.

You can pass arguments to its execution by using `args` or `:`, e.g:

    nim dev : -h

**Build**

    nim build

This will create a release build with name `multitalk` into `out/` folder.