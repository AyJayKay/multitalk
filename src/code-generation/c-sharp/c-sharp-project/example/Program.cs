﻿using System;
using System.IO;
using MultitalkGenerated;

class Program
{
	const string HeroName = "Anton";

	static void Main(string[] args)
	{
		var module = new MultitalkModule(OpenJsonContentByLanguage);

		module.LoadLanguage(MultitalkModule.Languages.English);

		// Serving suggestion :)
		var _ = module.Texts;

		Log(_.heroTitle); // "knight"

		// I recommend named parameters to benefit from code generation.
		Log(_.peopleShout(
			playerName: HeroName,
			title: _.heroTitle)
		);
		// "We want Anton the knight!"

		Log(_.quantity(-1)); // "none"
		Log(_.quantity(0));  // "none"

		Log(_.quantity(1));  // "some"
		Log(_.quantity(5));  // "some"

		Log(_.quantity(10)); // "a lot(10)"
		Log(_.quantity(100));// "a lot(100)"

		/*
		 * The purpose of Multitalk is to overcome dynamic access in code.
		 * Try to solve generated key name situations by using parameterized tokens.
		 * Anyways, access for dynamically retrieved key strings exists (e.g. using ids from backend):
		 */
		Log(_.DynamicGet("itemName_green")); // "Leaf"
		Log(_.DynamicGet("itemName_red"));   // "Blood"
		Log(_.DynamicGet("itemName_blue"));  // "Water"

		Log(_.DynamicGet("peopleShout", HeroName, _.heroTitle)); // "We want Anton the knight!"

		Log(_.DynamicGet("quantity", 5)); // "some"
	}

	// Load the data how you wish. (compression, resource extraction, etc.)
	private static StreamReader OpenJsonContentByLanguage(MultitalkModule.Languages language)
	{
		// Stream will be closed, automatically.
		return File.OpenText("./json-data/" + MultitalkModule.GetLanguageId(language) + ".json");
	}

	private static void Log(string msg)
	{
		Console.WriteLine(msg);
	}
}