using System;
using System.Collections.Generic;
using System.IO;
using Multitalk;

namespace MultitalkGenerated
{
	public class MultitalkModule
	{
		public enum Languages
		{
            German, English
		}

		public static class Keys
		{
            public const string heroTitle = "heroTitle";
            public const string peopleShout = "peopleShout";
            public const string quantity = "quantity";
            public const string itemName_green = "itemName_green";
            public const string itemName_red = "itemName_red";
            public const string itemName_blue = "itemName_blue";
		}

		public MultitalkTexts Texts { get; private set; }

		private readonly Func<Languages, StreamReader> _getDataByLanguageName;

		private static readonly string[] LANGUAGES =
		{
            "german", "english"
		};

		public static string GetLanguageId(Languages language)
		{
			return LANGUAGES[(int)language];
		}

		public MultitalkModule(Func<Languages, StreamReader> getDataByLanguageName)
		{
			_getDataByLanguageName = getDataByLanguageName;
		}

		public void LoadLanguage(Languages language)
		{
			var singleValueLocaTokens = new Dictionary<string, string>();
			var numericLocaTokens = new Dictionary<string, SortedList<int, string>>();

			MultitalkHelper.ReadJson(
				_getDataByLanguageName(language),
				ref singleValueLocaTokens,
				ref numericLocaTokens
			);

			Texts = new MultitalkTexts(singleValueLocaTokens, numericLocaTokens);
		}
	}

	public class MultitalkTexts
	{
		private readonly Dictionary<string, string> _singleValueLocaTokens;
		private readonly Dictionary<string, SortedList<int, string>> _numericLocaTokens;

		public MultitalkTexts(Dictionary<string, string> singleValueLocaTokens, Dictionary<string, SortedList<int, string>> numericLocaTokens)
		{
			_singleValueLocaTokens = singleValueLocaTokens;
			_numericLocaTokens = numericLocaTokens;
		}

		public string DynamicGet(string dynamicKey)
		{
			return MultitalkHelper.GetSimpleValue(_singleValueLocaTokens, dynamicKey);
		}

		public string DynamicGet(string dynamicKey, params string[] parameters)
		{
			return MultitalkHelper.GetParameterizedValue(_singleValueLocaTokens, dynamicKey, parameters);
		}

		public string DynamicGet(string dynamicKey, int amount)
		{
			return MultitalkHelper.GetNumericValue(_numericLocaTokens, dynamicKey, amount);
		}

    public string heroTitle { get { return MultitalkHelper.GetSimpleValue(_singleValueLocaTokens, MultitalkModule.Keys.heroTitle); } }
    public string peopleShout(string playerName, string title) { return MultitalkHelper.GetParameterizedValue(_singleValueLocaTokens, MultitalkModule.Keys.peopleShout, new string[]{playerName, title}); }
    public string quantity(int amount) { return MultitalkHelper.GetNumericValue(_numericLocaTokens, MultitalkModule.Keys.quantity, amount); }
    public string itemName_green { get { return MultitalkHelper.GetSimpleValue(_singleValueLocaTokens, MultitalkModule.Keys.itemName_green); } }
    public string itemName_red { get { return MultitalkHelper.GetSimpleValue(_singleValueLocaTokens, MultitalkModule.Keys.itemName_red); } }
    public string itemName_blue { get { return MultitalkHelper.GetSimpleValue(_singleValueLocaTokens, MultitalkModule.Keys.itemName_blue); } }
	}
}
