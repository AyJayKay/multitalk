using System;
using System.Collections.Generic;
using System.IO;
using Multitalk;

namespace __Ignore__
{
	public class MultitalkModule__ModuleName__
	{
		public enum Languages
		{
			//<language-enums>
			Language
			//<end-language-enums>
		}

		public static class Keys
		{
			//<keys>
			//ignore examples on code generation
			public const string myKey = "myKey";
			public const string myNumeric = "myNumeric";
			public const string myParameterized = "myParameterized";
			//<end-keys>
		}

		public MultitalkTexts__ModuleName__ Texts { get; private set; }

		private readonly Func<Languages, StreamReader> _getDataByLanguageName;

		private static readonly string[] LANGUAGES =
		{
			//<language-list>
			"language"
			//<end-language-list>
		};

		public static string GetLanguageId(Languages language)
		{
			return LANGUAGES[(int)language];
		}

		public MultitalkModule__ModuleName__(Func<Languages, StreamReader> getDataByLanguageName)
		{
			_getDataByLanguageName = getDataByLanguageName;
		}

		public void LoadLanguage(Languages language)
		{
			var singleValueLocaTokens = new Dictionary<string, string>();
			var numericLocaTokens = new Dictionary<string, SortedList<int, string>>();

			MultitalkHelper.ReadJson(
				_getDataByLanguageName(language),
				ref singleValueLocaTokens,
				ref numericLocaTokens
			);

			Texts = new MultitalkTexts__ModuleName__(singleValueLocaTokens, numericLocaTokens);
		}
	}

	public class MultitalkTexts__ModuleName__
	{
		private readonly Dictionary<string, string> _singleValueLocaTokens;
		private readonly Dictionary<string, SortedList<int, string>> _numericLocaTokens;

		public MultitalkTexts__ModuleName__(Dictionary<string, string> singleValueLocaTokens, Dictionary<string, SortedList<int, string>> numericLocaTokens)
		{
			_singleValueLocaTokens = singleValueLocaTokens;
			_numericLocaTokens = numericLocaTokens;
		}

		public string DynamicGet(string dynamicKey)
		{
			return MultitalkHelper.GetSimpleValue(_singleValueLocaTokens, dynamicKey);
		}

		public string DynamicGet(string dynamicKey, params string[] parameters)
		{
			return MultitalkHelper.GetParameterizedValue(_singleValueLocaTokens, dynamicKey, parameters);
		}

		public string DynamicGet(string dynamicKey, int amount)
		{
			return MultitalkHelper.GetNumericValue(_numericLocaTokens, dynamicKey, amount);
		}

		//<tokens>
		//ignore examples on code generation
		public string myKey { get { return MultitalkHelper.GetSimpleValue(_singleValueLocaTokens, MultitalkModule__ModuleName__.Keys.myKey); } }
		public string myNumeric(int amount) { return MultitalkHelper.GetNumericValue(_numericLocaTokens, MultitalkModule__ModuleName__.Keys.myNumeric, amount); }
		public string myParameterized(string name, string title) { return MultitalkHelper.GetParameterizedValue(_singleValueLocaTokens, MultitalkModule__ModuleName__.Keys.myParameterized, new[]{name, title}); }
		//<end-tokens>
	}
}