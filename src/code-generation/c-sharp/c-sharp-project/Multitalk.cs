using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Newtonsoft.Json;

namespace Multitalk
{
	static class MultitalkHelper
	{
		private const string MissingLocalizationString = "!MISSING LOCALIZATION!";

		public static void ReadJson(
			StreamReader dataStream,
			ref Dictionary<string, string> singleValueLocaTokens,
			ref Dictionary<string, SortedList<int, string>> numericLocaTokens
		)
		{
			using (dataStream)
			using (var jsonReader = new JsonTextReader(dataStream))
			{
				while (jsonReader.Read())
				{
					//start by key
					if (IsNotReadingKey(jsonReader))
						continue;

					var key = StringValue(jsonReader);

					//continue...
					jsonReader.Read();

					//...with simple value
					if (IsReadingSingleValue(jsonReader))
					{
						singleValueLocaTokens.Add(key, StringValue(jsonReader));
					}
					//...with numeric object value
					else if (IsReadingObjectValue(jsonReader))
					{
						var numericEntries = new SortedList<int, string>();
						numericLocaTokens.Add(key, numericEntries);

						while (jsonReader.TokenType != JsonToken.EndObject && jsonReader.Read())
						{
							//start by numeric key
							if (IsNotReadingKey(jsonReader)) continue;
							var numericKey = IntValue(jsonReader);

							//continue...
							jsonReader.Read();
							//...with string value
							if (IsReadingSingleValue(jsonReader))
							{
								numericEntries.Add(numericKey, StringValue(jsonReader));
							}
							else
							{
								throw new Exception("Found unexpected JSON data entry inside numeric localization token.");
							}
						}
					}
					else
					{
						throw new Exception("Found unexpected JSON data entry.");
					}
				}
			}
		}

		public static string GetSimpleValue(Dictionary<string, string> data, string key)
		{
			if (!data.ContainsKey(key)) return MissingLocalizationString;

			return data[key];
		}

		public static string GetNumericValue(Dictionary<string, SortedList<int, string>> data, string key, int amount)
		{
			if (!data.ContainsKey(key)) return MissingLocalizationString;

			var numericEntries = data[key];

			//extract value for fitting amount
			foreach (var pair in numericEntries.Reverse())
			{
				if (pair.Key <= amount)
				{
					//return with inserted amount value
					return pair.Value.Replace("{}", amount.ToString());
				}
			}

			//non was found desc -> amount is smaller than first -> clamp
			return numericEntries[0].Replace("{}", amount.ToString());
		}

		public static string GetParameterizedValue(Dictionary<string, string> data, string key, string[] parameters)
		{
			if (!data.ContainsKey(key)) return MissingLocalizationString;

			return String.Format(data[key], parameters);
		}

		private static bool IsNotReadingKey(JsonTextReader jsonReader)
		{
			return jsonReader.Value == null || jsonReader.TokenType != JsonToken.PropertyName;
		}

		private static bool IsReadingSingleValue(JsonTextReader jsonReader)
		{
			return jsonReader.Value != null && jsonReader.TokenType == JsonToken.String;
		}

		private static bool IsReadingObjectValue(JsonTextReader jsonReader)
		{
			return jsonReader.TokenType == JsonToken.StartObject;
		}

		private static string StringValue(JsonTextReader jsonReader)
		{
			return jsonReader.Value as string;
		}

		private static int IntValue(JsonTextReader jsonReader)
		{
			return Int32.Parse(StringValue(jsonReader));
		}
	}
}