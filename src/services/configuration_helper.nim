import os, strutils, tables, strformat
import echo_helper, ../data/command_options

var optionsContext: Table[CommandOptionIds, string]
proc setOptionsContext*(options: Table[CommandOptionIds, string]) = optionsContext = options
var ensuredOptions = newSeq[CommandOptionIds]()

const baseLanguage* = "english"
const sheetFileEnding = "txt"

proc getOptionValue(option: CommandOptionIds): string =
    if not (option in ensuredOptions):
        raiseException fmt"Option '{option}' is used but was not ensured!"
    return optionsContext[option]

proc getSheetsDirPath*(): string = getOptionValue(CommandOptionIds.sheet)
proc getExportSheetDirPath*(): string = getOptionValue(CommandOptionIds.sheetexport)
proc getImportSheetDirPath*(): string = getOptionValue(CommandOptionIds.sheetimport)
proc getSheetDataDirPath*(): string = getOptionValue(CommandOptionIds.data)
proc getCodeGenerationPath*(fileExtension: string): string = getOptionValue(CommandOptionIds.generated) / "Generated".addFileExt(fileExtension)

proc getSheetPath*(language: string): string = getSheetsDirPath() / language.addFileExt(sheetFileEnding)
proc getExportSheetPath*(language: string): string = getExportSheetDirPath() / language.addFileExt(sheetFileEnding)
proc getImportSheetPath*(language: string): string = getImportSheetDirPath() / language.addFileExt(sheetFileEnding)
proc getSheetDataPath*(language: string): string = getSheetDataDirPath() / language.addFileExt("json")

proc getSheetBaseLanguagePath*(): string = getSheetPath(baseLanguage)
proc getSheetDataBaseLanguagePath*(): string = getSheetDataPath(baseLanguage)

proc getAllLanguages*(): seq[string] =
    for kind, path in walkDir(getSheetsDirPath()):
        let fileName = path.extractFilename()
        if kind == pcFile and fileName.endsWith(sheetFileEnding):
            result.add(path.extractFilename().changeFileExt(""))

proc getLanguageOptionValues*(options: Table[CommandOptionIds, string]): seq[string] =
    let allLanguages = getAllLanguages()

    if options.hasKey(CommandOptionIds.language):
        let language = options[CommandOptionIds.language]
        if allLanguages.contains(language):
            return @[language]
        else:
            raiseArgumentException fmt"'{language}' is not a configured language!"
    else:
        log "No language was specified. All languages will be considered."
        return allLanguages

proc ensureOptionValue*(option: CommandOptionIds) =
    let value = optionsContext.getOrDefault(option, "")

    if value == "":
        raiseArgumentException fmt"Option '{option}' is needed for this command."
    if option in PATH_COMMAND_OPTION_IDS and not (fileExists(value) or dirExists(value)):
        raiseArgumentException fmt"Path of option '{option}' doesn't exist: {value}"
    
    ensuredOptions.add(option)