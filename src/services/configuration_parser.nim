import os, options, tables, parsecfg, strformat
import path_helper, echo_helper, arg_parser, path_helper
import ../data/[constants, parsed_arguments, command_options]

proc findDefaultFilePathUpwards(dirPath: string): Option[string] =
    if dirPath.isRootDir:
        return none(string)
    let defaultFilePath = dirPath / defaultConfigName
    if fileExists(defaultFilePath):
        return some(defaultFilePath)
    else:
        return findDefaultFilePathUpwards(dirPath.parentDir)

proc findConfigurationPath*(arguments: ParsedArguments, currentDirPath: string = getCurrentDir()): Option[string] =
    if arguments.options.hasKey(CommandOptionIds.config):
        var path = arguments.options[CommandOptionIds.config].getAbsoluteOrBasedOn(currentDirPath)
        
        if dirExists(path):
            path = path / defaultConfigName

        if fileExists(path):
            return some(path)
        else:
            raiseArgumentException "Couldn't find config at " & path
    
    else:
        return findDefaultFilePathUpwards(currentDirPath)

proc parseConfiguration*(path: string): Table[CommandOptionIds, string] =
    logHint "Using config: " & path
    let configObject = loadConfig(path)

    var configOptions: Table[CommandOptionIds, string]
    for _, subconfig in configObject.pairs:
        for key, value in subconfig:
            let option = parseCommandOptionId(key)
            if option.isSome():
                let optionValue = normalizePathOptionValue(option.get(), value, path.parentDir)
                configOptions[option.get()] = optionValue
            else:
                raiseArgumentException fmt"'{key}' is not a valid config option."
    
    return configOptions