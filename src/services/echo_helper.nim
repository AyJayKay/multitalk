import strformat, strutils, terminal, sequtils, re
import ../data/exceptions

export terminal.styledWriteLine

const indentationWidth = 4

var beQuiet = false
proc muteEchoHelper*() = beQuiet = true
proc unmuteEchoHelper*() = beQuiet = false

var currentIndentationLevel = 0
proc indentLvlPlus*(amount: int = 1) = currentIndentationLevel = (currentIndentationLevel + amount).clamp(0, 10)
proc indentLvlMinus*(amount: int = 1) = indentLvlPlus(-amount)
proc indentLvlZero*() = currentIndentationLevel = 0

proc indentBy*(texts: openArray[string], indentationLevel: int): string =
    texts.join("\n").indent(indentationWidth * indentationLevel)

proc indentBy*(text: string, indentationLevel: int): string = indentBy([text], indentationLevel)

proc indentAuto*(texts: openArray[string]): string = texts.indentBy(currentIndentationLevel)

proc indentAuto*(text: string): string = indentAuto([text])

proc bullitPoint*(text: string): string = fmt"# {text}"

proc bullitPoint*(text: openArray[string]): seq[string] = text.map(bullitPoint)

proc log*(texts: varargs[string]) =
    if not beQuiet:
        echo texts.join(" ")

proc extractIndentation(text: string): (string, string) =
    var matches: array[2, string]
    discard text.match(re"( *)(.*)", matches)
    return (matches[0], matches[1])

proc logHint*(texts: varargs[string]) =
    if not beQuiet:
        let (indentation, content) = extractIndentation(texts.join(""))
        styledEcho indentation, fgYellow, "Hint: ", resetStyle, content

proc logError*(texts: varargs[string]) =
    if not beQuiet:
        let (indentation, content) = extractIndentation(texts.join(""))
        styledEcho indentation, fgRed, "Error: ", resetStyle, content

template raiseException*(message: string) =
    raise newException(RuntimeException, message)

proc raiseException(exception: type, message: string = "") =
    raise newException(exception, message)

proc raiseValidationException*(message: string) = raiseException(ValidationException, message)

proc raiseArgumentException*(message: string) = raiseException(ArgumentParseException, message)

proc styledEchoException*(exception: ref Exception) =
    styledEcho fgRed, exception.msg, resetStyle, " [", $exception.name, "]"
    let stackTrace = exception.getStackTraceEntries()
    for i in countdown(stackTrace.len-1, 0):
        let trace = stackTrace[i]
        styledEcho "at ".indentBy(1), fgRed, $trace.procname, resetStyle, ":", $trace.line, styleDim, " (", $trace.filename, ")"

template logRuntimeException*() =
    if not beQuiet:
        let exception = getCurrentException()
        echo ""
        styledEchoException exception
        echo ""
        styledEcho styleDim, "Snap! This shouldn't have happened! Please report this exception message."