import strutils

proc toFirstUpper*(text: string): string = ($text[0]).toUpper & text.toLower[1..^1]

proc surround*(text, surrounding: string): string = surrounding & text & surrounding