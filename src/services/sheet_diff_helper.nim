import sequtils, sugar
import ../data/loca_token

type LocaTokenPair* = object
    oldToken*: LocaToken
    newToken*: LocaToken

proc findAddedLocaKeys*(baseTokens, targetTokens: seq[LocaToken]): seq[LocaToken] =
    baseTokens.filter(base => not targetTokens.anyIt(it.key == base.key))

proc findUpdatedLocaTokens*(newBaseTokens, oldBaseTokens: seq[LocaToken]): seq[LocaTokenPair] =
    for oldBaseToken in oldBaseTokens:
        for newBaseToken in newBaseTokens:
            if oldBaseToken.key == newBaseToken.key and oldBaseToken != newBaseToken:
                result.add(LocaTokenPair(oldToken: oldBaseToken, newToken: newBaseToken))
                break