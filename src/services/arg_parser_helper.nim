import sequtils, re, strformat
import ../data/command_options

# https://github.com/nim-lang/Nim/issues/13086
proc repairIgnoreArgumentBug*(args: seq[string]): seq[string] =
    proc containsOption(list: seq[auto], option: string): bool =
        return list.anyIt(option.match(re(fmt"--?{$it}[:=]?$")))

    var i = 0
    while i < args.len:
        let currentArg = args[i]
        if i == args.len - 1:
            result.add(currentArg)
        elif HAS_VALUE_SHORTS.containsOption(currentArg):
            result.add(currentArg & args[i + 1])
            i += 1
        elif HAS_VALUE_LONGS.containsOption(currentArg):
            result.add(currentArg & "=" & args[i + 1])
            i += 1
        else:
            result.add(currentArg)
        i += 1