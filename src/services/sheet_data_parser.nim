import streams, json, strformat
import echo_helper
import ../data/[loca_token, constants]

proc parseSheetData*(stream: Stream): seq[LocaToken] =
    let sheetData = parseJson(stream)

    for key, value in sheetData.pairs:
        let isSimpleLocaToken = value.kind == JsonNodeKind.JString
        let isNumericToken = value.kind == JsonNodeKind.JObject

        if isSimpleLocaToken:
            let parsedToken = SimpleLocaToken(key: key, value: value.getStr())

            if parsedToken.matchesParameterized(parameterInDataValueRegex):
                result.add(parsedToken.toParameterized(parameterInDataValueRegex))
            else:
                result.add(parsedToken)

        elif isNumericToken:
            var parsedToken = NumericLocaToken(key: key, values: newSeq[(string, string)]())

            for numKey, numValue in value.pairs:
                if numValue.kind != JsonNodeKind.JString:
                    raiseException fmt"Numeric loca token '{key}' has invalid content."

                parsedToken.values.add((numKey, numValue.getStr()))

            result.add(parsedToken)

        else:
            raiseException fmt"Json sheet data parser found unknown token type with key '{key}'."

proc parseSheetData*(filePath: string): seq[LocaToken] =
    var stream = newFileStream(filePath, fmRead)
    if stream != nil:
        try:
            return parseSheetData(stream)
        except:
            raise
        finally:
            stream.close()
    
    return newSeq[LocaToken]()