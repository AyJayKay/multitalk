import strutils, sequtils
import echo_helper, configuration_helper, string_helper
import ../data/loca_token
import ../data/code-generation/c_sharp

const modulePlaceHolder = "__ModuleName__"
const oldNamespaceLineStart = "namespace"
const namespaceLine = oldNamespaceLineStart & " MultitalkGenerated"

type CodeContext = enum
    normal,
    skipping,
    keyId = "keys",
    langEnumId = "language-enums",
    langListId = "language-list",
    tokenId = "tokens"

proc asTag(tagName: CodeContext): string = "//<" & $tagName & ">"
proc asEndTag(tagName: CodeContext): string = "//<end-" & $tagName & ">"

proc detectCodeContext(currentLine: string, currentCodeContext: CodeContext): CodeContext =
    case currentCodeContext:
        of normal:
            for context in CodeContext.keyId..CodeContext.high:
                if context.asTag in currentLine:
                    return CodeContext.skipping
        of skipping:
            for context in CodeContext.keyId..CodeContext.high:
                if context.asEndTag in currentLine:
                    return context
        of CodeContext.keyId..CodeContext.high:
            return CodeContext.normal
    
    return currentCodeContext

proc buildLanguageEnumLine(languages: seq[string]): string =
    languages
        .map(toFirstUpper)
        .join(", ")
        .indentBy(3)

proc buildLanguageListLine(languages: seq[string]): string =
    languages
        .join("\", \"")
        .surround("\"")
        .indentBy(3)

proc buildKeyLine(key: string): string =
    "public const string #key# = \"#key#\";"
        .replace("#key#", key)
        .indentBy(3)

proc buildParameterizedTokenLine(token: ParameterizedLocaToken): string =
    let functionParams = "string " & token.parameterNames.join(", string ")
    let paramsList = token.parameterNames.join(", ")

    "public string #key#(#functionParams#) { return MultitalkHelper.GetParameterizedValue(_singleValueLocaTokens, MultitalkModule.Keys.#key#, new string[]{#paramsList#}); }"
        .replace("#functionParams#", functionParams)
        .replace("#paramsList#", paramsList)
        .replace("#key#", token.key)
        .indentBy(1)

proc buildSimpleTokenLine(key: string): string =
    "public string #key# { get { return MultitalkHelper.GetSimpleValue(_singleValueLocaTokens, MultitalkModule.Keys.#key#); } }"
        .replace("#key#", key)
        .indentBy(1)

proc buildNumericTokenLine(key: string): string =
    "public string #key#(int amount) { return MultitalkHelper.GetNumericValue(_numericLocaTokens, MultitalkModule.Keys.#key#, amount); }"
        .replace("#key#", key)
        .indentBy(1)

proc generateCSharp*(sheet: seq[LocaToken]) =
    var file = open(getCodeGenerationPath("cs"), fmWrite)
    let allLanguages = getAllLanguages()
    try:
        var currentCodeContext = CodeContext.normal

        for line in cSharpCodeSource.splitLines:
            currentCodeContext = line.detectCodeContext(currentCodeContext)
            case currentCodeContext:
                of normal:
                    if line.startsWith(oldNamespaceLineStart):
                        file.writeLine(namespaceLine)
                    else:
                        file.writeLine(line.replace(modulePlaceHolder, "")) #TODO module name implementation
                
                of skipping:
                    continue
                
                of keyId:
                    for token in sheet:
                        file.writeLine(buildKeyLine(token.key))
                
                of langEnumId:
                    file.writeLine(buildLanguageEnumLine(allLanguages))
                
                of langListId:
                    file.writeLine(buildLanguageListLine(allLanguages))
                
                of tokenId:
                    for token in sheet:
                        # order matters as (parameterized is simple) == true
                        if token of ParameterizedLocaToken:
                            file.writeLine(buildParameterizedTokenLine(ParameterizedLocaToken(token)))
                        elif token of SimpleLocaToken:
                            file.writeLine(buildSimpleTokenLine(token.key))
                        elif token of NumericLocaToken:
                            file.writeLine(buildNumericTokenLine(token.key))
                        else:
                            raiseException "Code generator found unhandled token type."
    except:
        raise
    finally:
        file.close()
