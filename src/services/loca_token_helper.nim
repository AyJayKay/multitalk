import re, sugar, sequtils
import ../data/[loca_token, constants]

proc populateComment*(token: LocaToken, comments: varargs[string]): LocaToken =
    token.comments &= @comments
    return token

proc removeToolGeneratedComments*(token: LocaToken): LocaToken =
    var comments = newSeq[string]()
    block loop:
        for comment in token.comments:
            for commentConstant in low(CommentConst)..high(CommentConst):
                if comment == $commentConstant:
                    break loop
            comments.add(comment)

    token.comments = comments
    return token

proc insertParameterNames*(tokenValue: string, parameterNames: seq[string]): string =
    result = tokenValue
    for i, param in parameterNames.pairs:
        result = result.replace(re("\\{" & $i & "\\}"), "{" & param & "}")

proc insertSingleParameterName*(tokenValue: string, parameterName: string): string =
    return tokenValue.replace(re"\{\d\}", "{" & parameterName & "}")

proc mergeAndReplaceWith*(targetTokens, newOrUpdatedTokens: seq[LocaToken]): seq[LocaToken] =
    result = targetTokens.filter(target => not newOrUpdatedTokens.anyIt(it.key == target.key))
    result &= newOrUpdatedTokens