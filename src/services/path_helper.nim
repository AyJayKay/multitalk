import os, strformat
import echo_helper, ../data/command_options

proc getAbsoluteOrBasedOn*(path, base: string): string =
    if path.isAbsolute():
        return path.normalizedPath

    if not base.isAbsolute():
        raiseException fmt"Base path has to be absolute ({base})"
    
    return base / path

proc normalizePathOptionValue*(optionId: CommandOptionIds, value, basePath: string): string =
    #TODO add validation for file/dir type on option clean up changes
    if optionId in PATH_COMMAND_OPTION_IDS:
        value.getAbsoluteOrBasedOn(basePath)
    else:
        value