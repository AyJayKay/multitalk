import json
import echo_helper
import ../data/loca_token

proc generateSheetData*(tokens: seq[LocaToken]): JsonNode =
    var json = parseJson("{}")
    for token in tokens:
        if token of SimpleLocaToken:
            json{token.key} = newJString(SimpleLocaToken(token).value)

        elif token of NumericLocaToken:
            for numKey, numValue in NumericLocaToken(token).values.cleanDotDotNotation().items:
                json{token.key, numKey} = newJString(numValue)

        else:
            raiseException "Sheet data generation found unhandled loca token type!"

    return json