import streams, strformat, re, os
import sheet_parser, echo_helper, configuration_helper, validation_helper
import ../data/[loca_token, constants, command_options]

proc addLocaKey*(keyName: string) =
    keyName.validateLocaTokenKeyName()
    ensureOptionValue(CommandOptionIds.sheet)

    let tokens = parseSheet(getSheetBaseLanguagePath())
    keyName.validateFreeLocaTokenKeyNameIn(tokens)

    var file = newFileStream(getSheetBaseLanguagePath(), fmAppend)
    if file != nil:
        try:
            file.writeLine("\n")
            file.writeLine(fmt"{keyName}: {q}{missingLocalizationConst}{q}")
        except:
            raise
        finally:
            file.close()

proc addLocaTokens*(language: string, tokens: varargs[LocaToken]) =
    ensureOptionValue(CommandOptionIds.sheet)

    var file = newFileStream(getSheetPath(language), fmAppend)
    if file != nil:
        try:
            file.writeLine("\n")
            for token in tokens:
                file.writeLine("\n" & token.renderRawFull())
        except:
            raise
        finally:
            file.close()

proc createSheet*(language, path: string, tokens: seq[LocaToken] = @[]) =
    var file = newFileStream(path, fmWrite)
    if file != nil:
        try:
            file.writeLine(fmt"# [{language}]")
            for token in tokens:
                file.writeLine("\n" & $token)
        except:
            raise
        finally:
            file.close()

proc addLanguageFile*(language: string) =
    ensureOptionValue(CommandOptionIds.sheet)

    let newLanguagePath = getSheetPath(language)

    if existsFile(newLanguagePath):
        raiseValidationException fmt"Language '{language}' is already added!"

    if not language.match(re(keyRegexString)):
        raiseValidationException fmt"'{language}' is not a valid language name!"

    log "Creating sheet: " & newLanguagePath
    createSheet(language, newLanguagePath)