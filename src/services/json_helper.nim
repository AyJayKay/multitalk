import streams, json

proc writeJson*(json: JsonNode, path: string) =
    var jsonString = ""
    toUgly(jsonString, json)

    var file = newFileStream(path, fmWrite)
    if file != nil:
        try:
            file.writeLine(jsonString)
        except:
            raise
        finally:
            file.close()