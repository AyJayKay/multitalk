import parseopt, options, tables, sequtils, strformat, sugar, os
import arg_parser_helper, functional_helper, echo_helper, path_helper
import ../data/[parsed_arguments, commands, command_options]
export ParsedArguments

type
    UnparsedArgument = tuple[kind: CmdLineKind; key, val: TaintedString]

proc getUnparsedArguments(programArguments: varargs[string]): seq[UnparsedArgument] =
    var argumentHandler = initOptParser(
        @programArguments.repairIgnoreArgumentBug(),
        NO_VALUE_SHORTS.toSet(),
        NO_VALUE_LONGS
    )
    return toSeq(argumentHandler.getopt())

proc validateParseArguments(arguments: seq[UnparsedArgument]): ParsedArguments =
    var errors = newSeq[string]()
    var hints = newSeq[string]()
    var command = none(CommandIds)
    var options = initTable[CommandOptionIds, string]()
    
    var optionsTail = arguments

    if arguments.first().filter(arg => arg.kind == CmdLineKind.cmdArgument).isSome():
        let commandName = arguments.first().map(arg => arg.key)
        command = commandName.flatMap(parseCommandId)
        if commandName.isSome() and command.isNone():
            errors.add(fmt"'{commandName.get()}' is not a valid command.")
        else:
            optionsTail = arguments.tail()
    else:
        errors.add("No command was specified.")

    proc isValueKey(argKey: string): bool =
        return HAS_VALUE_SHORTS.anyIt(argKey == $it) or HAS_VALUE_LONGS.anyIt(argKey == it)
    
    proc isNonValueKey(argKey: string): bool =
        return NO_VALUE_SHORTS.anyIt(argKey == $it) or NO_VALUE_LONGS.anyIt(argKey == it)

    for arg in optionsTail:
        case arg.kind
        of cmdArgument:
            errors.add(fmt"Only one argument is allowed (The command). Found lonely value '{arg.key}'.")
        of cmdShortOption, cmdLongOption:
            let isValueKey = isValueKey(arg.key)
            let isNonValueKey = isNonValueKey(arg.key)

            if isNonValueKey and arg.val.len > 0:
                errors.add(fmt"'{arg.key}' is a non value option. Found value '{arg.val}'.")
            
            if isValueKey:
                if arg.val.len == 0:
                    errors.add(fmt"Option '{arg.key}' needs to have a value.")
            
            let optionId = parseCommandOptionId(arg.key)
            if optionId.isSome():
                if options.hasKey(optionId.get()) and isValueKey:
                    hints.add(fmt"Option '{arg.key}' is defined multiple times. Last value is used.")

                let optionValue = normalizePathOptionValue(optionId.get(), arg.val, getCurrentDir())

                options[optionId.get()] = optionValue
            else:
                errors.add(fmt"'{arg.key}' is not a valid option.")   
        else: discard

    return ParsedArguments(errors: errors, hints: hints, command: command, options: options)

proc parseCommandLineArguments*(arguments: openArray[string] = commandLineParams()): ParsedArguments =
    arguments.getUnparsedArguments().validateParseArguments()

proc echoArgumentParsingLogs*(arguments: ParsedArguments) =
    for e in arguments.errors:
        logError e

    for h in arguments.hints:
        logHint h
    
    if arguments.errors.len > 0:
        raiseArgumentException ""

proc populateArgumentsByConfig*(arguments: var ParsedArguments, configArguments: Table[CommandOptionIds, string]) =
    for key, value in configArguments:
        if not arguments.options.hasKey(key):
            arguments.options[key] = value