import options, sequtils

proc toSet*(sequence: seq[char]): set[char] = sequence.mapIt({it}).foldl(a + b)

proc first*[T](sequence: seq[T]): Option[T] =
    if sequence.len == 0:
        return none(T)
    else:
        return some(sequence[0])

proc last*[T](sequence: seq[T]): Option[T] =
    if sequence.len == 0:
        return none(T)
    else:
        return some(sequence[sequence.len - 1])

proc front*[T](sequence: seq[T]): seq[T] =
    if sequence.len <= 1:
        return sequence
    else:
        return sequence[0..^2]

proc tail*[T](sequence: seq[T]): seq[T] =
    if sequence.len <= 1:
        return newSeq[T]()
    else:
        return sequence[1..^1]

proc findFirst*[T](sequence: seq[T], f: proc(_: T):bool): Option[T] =
    for element in sequence:
        if f(element):
            return some(element)
    return none(T)

proc flattened*[T](sequence: seq[Option[T]]): seq[T] =
    for elem in sequence:
        if elem.isSome():
            result.add(elem.get())