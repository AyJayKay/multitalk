import re, strformat, sequtils, strutils, algorithm
import echo_helper
import ../data/[loca_token, constants]

proc validateLocaTokenKeyName*(keyName: string) =
    if reservedKeyWords.contains(keyName):
        raiseValidationException fmt"Key name '{keyName}' is reserved! Alter casing or find a different name."

    if not keyName.match(re("^" & keyRegexString & "$")):
        raiseValidationException fmt"Key name '{keyName}' contains invalid characters!"
    
    if keyName.startsWith(re"_[^\d]"):
        raiseValidationException fmt"Key name '{keyName}' may not start with '_'! Only number keys may start with underscores."
    
    if keyName.startsWith(re"\d"):
        raiseValidationException fmt"Key name '{keyName}' may not start with a didget! Start with underscore, if you really want to name a token with a number."

proc validateFreeLocaTokenKeyNameIn*(keyName: string, tokens: seq[LocaToken]) =
    if tokens.anyIt(it.key == keyName):
        raiseValidationException fmt"A key with name '{keyName}' already exists!"

proc validateUniqueTokenNames*(tokens: seq[LocaToken]): seq[LocaToken] =
    var duplicateTokens = newSeq[LocaToken]()

    let length = tokens.len()
    for i in 0..(length-2):
        let token = tokens[i]
        for j in (i+1)..(length-1):
            if token.key == tokens[j].key:
                duplicateTokens.add(token)
                break
    
    if duplicateTokens.len() > 0:
        for token in duplicateTokens:
            logError fmt"Token '{token.key}' is defined multiple times!".indentAuto()
        raiseValidationException "At least one token is defined multiple times in this sheet!"
    else:
        return tokens

proc validateNumericEntriesNonAmbiguousDotDotNotation(token: NumericLocaToken) =
    let entries = token.values
    let count = entries.len()
    for i in 1..<count:
        if entries[i][0] == ".." and entries[i - 1][0] == "..":
            raiseValidationException fmt"Numeric token '{token.key}' contains multiple '..'-notation definitions that follow each other!"

proc validateNumericEntriesOrder(token: NumericLocaToken) =
    var last = low(int)
    for entry in token.values:
        if entry[0] == "..":
            continue

        let number = entry[0].parseInt()
        if number > last:
            last = number
        else:
            raiseValidationException fmt"Numeric token '{token.key}' contains unsorted or non-unique numeric definitions! Make sure they go from low to high."

proc validateNumericLocaToken*(token: NumericLocaToken) =
    if token.values.len() == 0:
        raiseValidationException fmt"Numeric token '{token.key}' is empty!"
    elif token.values.len() > 1:
        validateNumericEntriesNonAmbiguousDotDotNotation(token)
        validateNumericEntriesOrder(token)

proc validateSimpleAndParameterizedLocaToken*(token: SimpleLocaToken) =
    if token.value == "" or token.value == missingLocalizationConst:
        logHint fmt"Remember to fill value of token '{token.key}' with the correct value :)".indentAuto()
    
    if token.value.contains(re"\{\d\w*\}") and not (token of ParameterizedLocaToken):
        logHint fmt"Token '{token.key}' contains something that looks like a parameter. Parameter names may not start with a number.".indentAuto()

proc findNotYetDefinedTargetTokens(targetTokens, baseTokens: seq[LocaToken]): seq[LocaToken] =
    for baseToken in baseTokens:
        if not targetTokens.anyIt(it.key == baseToken.key):
            result.add(baseToken)

proc validateCompareToBaseLanguageTokens*(targetTokens, baseTokens: seq[LocaToken], filterIgnoredTokens: bool = false): seq[LocaToken] =
        var missmatchingTypeTokens = newSeq[LocaToken]()
        var missmatchingParameterizedTokens = newSeq[ParameterizedLocaToken]()
        var ignoredTokens = newSeq[LocaToken]()

        for targetToken in targetTokens:
            var foundInBaseTokens = false

            for baseToken in baseTokens:
                if targetToken.key == baseToken.key:
                    foundInBaseTokens = true

                    if not targetToken.typeEquals(baseToken):
                        missmatchingTypeTokens.add(targetToken)

                    elif targetToken of ParameterizedLocaToken:
                        let targetParamToken = ParameterizedLocaToken(targetToken)
                        let baseParamToken = ParameterizedLocaToken(baseToken)
                        if targetParamToken.parameterNames.sorted() != baseParamToken.parameterNames.sorted():
                            missmatchingParameterizedTokens.add(targetParamToken)

                    break

            if not foundInBaseTokens:
                ignoredTokens.add(targetToken)

            if filterIgnoredTokens and foundInBaseTokens or not filterIgnoredTokens:
                result.add(targetToken)

        let notYetDefinedTokens = findNotYetDefinedTargetTokens(targetTokens, baseTokens)

        var isValidationFailed = false
        template validationFailureOn(condition: bool, solveHint: string) =
            if condition:
                logHint solveHint
                isValidationFailed = true

        for undefinedToken in notYetDefinedTokens:
            logHint fmt"Token '{undefinedToken.key}' is not set up for this language, yet. Don't forget it :)".indentAuto()

        for ignoredToken in ignoredTokens:
            logHint fmt"Token '{ignoredToken.key}' is not listed in the base language sheet. It will be ignored!".indentAuto()
        
        for missmatchingToken in missmatchingTypeTokens:
            logError fmt"Token '{missmatchingToken.key}' has the wrong type!".indentAuto()
        
        validationFailureOn missmatchingTypeTokens.len() > 0,
            "All tokens with the same key name across sheets need to match in token type.".indentAuto()
        
        for missmatchingToken in missmatchingParameterizedTokens:
            logError fmt"Parameterized token '{missmatchingToken.key}' contains invalid parameter setup!".indentAuto()

        validationFailureOn missmatchingParameterizedTokens.len() > 0,
            "All parameterized tokens with the same key name across sheets need to match the names and number of their parameters. (The order may differ. The amount may only differ because of parameter reuse.)".indentAuto()
        
        if isValidationFailed:
            raiseValidationException "At least one token definition of this sheet is invalid, compared to the base language sheet!"