import streams, strutils, re, strformat, options, sugar
import echo_helper, validation_helper, loca_token_helper
import ../data/loca_token

const generalKeyRegexString = "[^: ]*"
const whitespaceRegexString = "[ \t]"
const commentRegexString = "#"
const quoteStartRegexString = "[\"'`´«‹“‘]"
const quoteEndRegexString = "[\"'`´»›”’]"

proc startRe(regex: string): Regex = re("^" & regex & "+")
proc endRe(regex: string): Regex = re(regex & "+$")

proc removeBackSlashBefore(source, regex: string): string =
    #remove backslash prefix from target, if not prefixed with \ by itself
    source.replace(re(fmt"(?<!\\)\\(?={regex})"))

proc replaceAndEscape(text: string): string = text
    #trim
    .replace(startRe(whitespaceRegexString))
    .replace(endRe(whitespaceRegexString))
    #escaped line breaks
    .replace("\\n","\n")
    #secured, non-parameter braces
    .removeBackSlashBefore("{")
    .removeBackSlashBefore("}")
    #escaped comment sign
    .removeBackSlashBefore("#")
    #remove escape slash before quotes
    .removeBackSlashBefore(quoteStartRegexString)
    .removeBackSlashBefore(quoteEndRegexString)
    #escaped backslashes
    .removeBackSlashBefore("\\\\")

proc replaceAndEscape(token: LocaToken): LocaToken =
    if token of SimpleLocaToken:
        var simpleToken = SimpleLocaToken(token)
        simpleToken.value = simpleToken.value.replaceAndEscape
        return simpleToken
    return token

proc removeQuotes(text: string): string =
    if text.contains(startRe(quoteStartRegexString)) and text.contains(endRe(quoteEndRegexString)):
        return text.replace(startRe(quoteStartRegexString)).replace(endRe(quoteEndRegexString))
    return text

proc removeQuotes(token: LocaToken): LocaToken =
    if token of SimpleLocaToken:
        var simpleToken = SimpleLocaToken(token)
        simpleToken.value = simpleToken.value.removeQuotes
        return simpleToken
    return token

proc validateToken(token: LocaToken): LocaToken =
    if token of SimpleLocaToken:
        validateSimpleAndParameterizedLocaToken(SimpleLocaToken(token))
    elif token of NumericLocaToken:
        validateNumericLocaToken(NumericLocaToken(token))
    elif token is LocaToken:
        raiseValidationException fmt"Token '{token.key}' has no value!"
    else:
        raiseException "Validation of parsed token found unexpected token type!"
    return token

proc transformToParameterizedIfNeeded(token: LocaToken): LocaToken =
    if token.matchesParameterized:
        return SimpleLocaToken(token).toParameterized
    return token

proc parseKeyValueStructure(escapedLine: string): (string, string) =
    var matches: array[2, string]
    if escapedLine.match(re(fmt"({generalKeyRegexString}){whitespaceRegexString}*:{whitespaceRegexString}*(.*)$"), matches):
        return (matches[0], matches[1])
    else:
        return ("", "")

proc parseSheet*(sheet: Stream): seq[LocaToken] =
    var output = newSeq[LocaToken]()
    var insideToken = none(LocaToken)

    var lastCommentGroup = newSeq[string]()
    var currentAggregatingComments = newSeq[string]()
    proc lockCommentGroupForNextToken() =
        lastCommentGroup = currentAggregatingComments
        currentAggregatingComments = newSeq[string]()

    proc completeStructure() =
        insideToken.map((token: LocaToken) => token
                .transformToParameterizedIfNeeded()
                .removeQuotes()
                .replaceAndEscape()
                .validateToken()
                .populateComment(lastCommentGroup)
                .removeToolGeneratedComments()
            )
            .map((token: LocaToken) => output.add(token))

        insideToken = none(LocaToken)

    var line: string
    while sheet.readLine(line):
        let isIndented = line.contains(startRe(whitespaceRegexString))
        if isIndented:
            line = line.replace(startRe(whitespaceRegexString))

        if line.contains(startRe(commentRegexString)):
            currentAggregatingComments.add(line.replace(re(commentRegexString & whitespaceRegexString & "*")))
            continue
        elif line == "":
            currentAggregatingComments = newSeq[string]()
            continue

        let (key, value) = parseKeyValueStructure(line)
        let isKeyValueStructure = key.len() > 0

        # completing former substructure
        if insideToken.isSome() and not isIndented and isKeyValueStructure:
            completeStructure()
        
        if insideToken.isNone(): # currently parsing at sheet root
            if not isKeyValueStructure:
                raiseValidationException fmt"'{line}' is not a valid loca definition."

            key.validateLocaTokenKeyName()

            if isIndented:
                raiseValidationException fmt"Found indentation but expected a valid root definition at '{line}'."

            lockCommentGroupForNextToken()

            if value == "": # line is a structure entry
                insideToken = some(LocaToken(key: key))
                continue
            else: # text value token
                insideToken = some(LocaToken(SimpleLocaToken(key: key, value: value)))
                continue

        else: # currently parsing a substructure
            let isMultilineToken = insideToken.get() of SimpleLocaToken
            let isNumericToken = insideToken.get() of NumericLocaToken
            let isNotYetDefinedToken = not isMultilineToken and not isNumericToken
            let isValidNumericDefinitionLine = key.match(re"(\d+|\.\.)") and value.len > 0

            if isValidNumericDefinitionLine:
                if not isNumericToken and not isNotYetDefinedToken:
                    raiseValidationException fmt"'{line}' is a numeric definition inside a different structure."

                var newNumericToken =
                    if isNumericToken: NumericLocaToken(insideToken.get())
                    else: NumericLocaToken(key: insideToken.get().key, values: newSeq[(string, string)]())

                let numericValue = value.replaceAndEscape.removeQuotes
                newNumericToken.values.add((key, numericValue))

                insideToken = some(LocaToken(newNumericToken))
                continue
            
            else:
                if not isKeyValueStructure: # simple string definition
                    if not isMultilineToken and not isNotYetDefinedToken:
                        raiseValidationException fmt"'{line}' is a plain string definition inside a numeric token structure."
                        continue
                    
                    var newSimpleToken = 
                        if isMultilineToken: SimpleLocaToken(insideToken.get())
                        else: SimpleLocaToken(key: insideToken.get().key)

                    if isMultilineToken:
                        newSimpleToken.value &= "\n"
                    newSimpleToken.value &= line

                    insideToken = some(LocaToken(newSimpleToken))
                    continue

                else: #end of structure
                    raiseException "End of structure was not detected."

        raiseException "Sheet parser run out of options."
    
    # complete last substructure
    if insideToken.isSome():
        completeStructure()
    
    return output

proc parseSheet*(filePath: string): seq[LocaToken] =
    var stream = newFileStream(filePath, fmRead)
    if stream != nil:
        try:
            return parseSheet(stream)
        except:
            raise
        finally:
            stream.close()
    
    return newSeq[LocaToken]()