import options
import command
import ../commands/generateCommand, ../commands/addCommand, ../commands/exportCommand, ../commands/importCommand

type CommandIds* = enum
    generate
    add
    `export`
    `import`

const COMMANDS*: seq[Command] = @[
    newCommand($CommandIds.add, addCommand.execute, [], addCommand.descriptions),
    newCommand($CommandIds.`export`, exportCommand.execute, [], exportCommand.descriptions),
    newCommand($CommandIds.`import`, importCommand.execute, [], importCommand.descriptions),
    newCommand($CommandIds.generate, generateCommand.execute, [], generateCommand.descriptions)
]

proc parseCommandId*(idName: string): Option[CommandIds] =
    for id in CommandIds.items:
        if $id == idName:
            return some(id)
    return none(CommandIds)