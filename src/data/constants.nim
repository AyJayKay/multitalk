import re

type CommentConst* {.pure.} = enum
    updatedToken = "UPDATED"
    newToken = "NEW"

const q* = "\""
const keyRegexString* = "[a-zA-Z0-9_-]+"
const reservedKeyWords* = @[
    "DynamicGet",
    "_"
]
const defaultConfigName* = "multitalk.conf"
const missingLocalizationConst* = "!MISSING LOCALIZATION!"

proc getParameterInStringRegex*(content: string): Regex = re("""(?<!\\)(?<={)""" & content & """(?=\})""")
let parameterInValueRegex* = getParameterInStringRegex("""[a-zA-Z_][\w\d]*""")
let parameterInDataValueRegex* = getParameterInStringRegex("""\d*""")