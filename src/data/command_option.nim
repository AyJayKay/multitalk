import options, sequtils, sugar

type
    ValueType* {.pure.} = enum
        none
        text
        filePath
        dirPath
        list

    CommandOption* = object
        name*: string
        alternatives*: seq[string]
        short*: Option[char]
        description*: string
        valueType*: ValueType

proc newCommandOption*(
        name: string,
        description: string,
        short: char = ' ',
        alternatives: openArray[string] = [],
        valueType: ValueType = ValueType.text
    ): CommandOption = return CommandOption(
        name: name,
        alternatives: @alternatives,
        short: if short != ' ': some(short) else: none(char),
        description: description,
        valueType: valueType
    )

proc hasValue*(commandOption: CommandOption): bool = commandOption.valueType != ValueType.none

proc longOptions*(commandOption: CommandOption, dashed: bool = false) : seq[string] =
    let options = (commandOption.name & commandOption.alternatives).deduplicate
    if dashed:
        return options.mapIt("--" & it)
    else:
        return options

proc allOptions*(commandOption: CommandOption, dashed: bool = false) : seq[string] =
    let shortOption = commandOption.short.map(it => (if dashed: "-" else: "") & $it)
    if shortOption.isSome():
        return shortOption.get() & commandOption.longOptions(dashed)
    else:
        return commandOption.longOptions(dashed)