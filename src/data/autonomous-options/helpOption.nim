import strutils, sequtils, strformat
import ../../services/echo_helper
import ../../data/[commands, command_option, command_options, loca_token]

proc showHelp*() =
    log "Usage:"
    log "multitalk <command> [ --<option> | --<option>=<value> ... ]".indentBy(1)

    log "Commands:"
    for command in COMMANDS:        
        var descriptions: seq[string] = command.descriptions

        let requiredOptionsString = command.requiredOptions.mapIt($it).join(", ")
        if command.requiredOptions.len == 1:
            descriptions.add(fmt"Required option: {requiredOptionsString}")
        elif command.requiredOptions.len > 1:
            descriptions.add(fmt"Required options: {requiredOptionsString}")

        log fmt"{command.name}:".indentBy(1)
        log descriptions.map(bullitPoint).indentBy(2)
    
    log "Options:"
    var optionNamesLines = newSeq[string]()
    for option in COMMAND_OPTIONS:
        let optionNames = option.allOptions(true)
        let optionNamesText =
            if optionNames.len > 1: "( " & optionNames.join(" | ") & " )"
            else: optionNames[0]
        
        let optionValueType =
            if option.hasValue: "=<" & option.description.split(" ")[0].toLower() & ">"
            else: ""

        optionNamesLines.add(optionNamesText & optionValueType)

    let optionNamesWidth = optionNamesLines.mapIt(it.len).max
    
    for i, option in COMMAND_OPTIONS.pairs:
        let descriptionOffset = optionNamesWidth - optionNamesLines[i].len + 1
        log optionNamesLines[i].indentBy(1), option.description.bullitPoint().indent(descriptionOffset)

    log "Token types:"
    for (name, description) in locaTokenDescriptions:
        log fmt"{name}:".indentBy(1)
        log fmt"{description}".bullitPoint().indentBy(2)
    
    log "Option value syntax:"
    log [
        "--option=value",
        "--option:value",
        "--option value",
        "-o=value",
        "-o:value",
        "-o value",
        "-ovalue"
    ].indentBy(1)

    log "for list values:".indentBy(1)
    log [
        "-o=value1,value2,...",
        "-o=\'value1 value2 ...\'",
        "-o=\'value1, value2, ...\'"
    ].indentBy(2)