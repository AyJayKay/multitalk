import options, sequtils, sugar
import command_option
import ../services/functional_helper

type CommandOptionIds* = enum
    help
    version
    quiet
    config
    sheet
    sheetexport
    sheetimport
    generated
    data
    language
    token
    `type`

const COMMAND_OPTIONS*: seq[CommandOption] = @[
    newCommandOption($CommandOptionIds.help, "You already found out :P", 'h', valueType = ValueType.none),
    newCommandOption($CommandOptionIds.version, "prints simple version string", 'v', valueType = ValueType.none),
    newCommandOption($CommandOptionIds.quiet, "disables console output (watch error codes)", 'q', valueType = ValueType.none),
    newCommandOption($CommandOptionIds.config, "path to config file (r)", 'c', valueType = ValueType.filePath),
    newCommandOption($CommandOptionIds.sheet, "path to sheets folder (r+w)", 's', ["sheets"], valueType = ValueType.dirPath),
    newCommandOption($CommandOptionIds.sheetexport, "path to folder for translator (w)", 'e', ["export"], valueType = ValueType.dirPath),
    newCommandOption($CommandOptionIds.sheetimport, "path to folder from translator (r+w)", 'i', ["import"], valueType = ValueType.dirPath),
    newCommandOption($CommandOptionIds.generated, "path to generated code folder (w)", 'g', valueType = ValueType.dirPath),
    newCommandOption($CommandOptionIds.data, "path to generated data folder (r+w)", 'd', valueType = ValueType.dirPath),
    newCommandOption($CommandOptionIds.language, "list of languages/language to consider", 'l', ["languages"], valueType = ValueType.list),
    newCommandOption($CommandOptionIds.token, "name of token to consider", 'n', ["key"]),
    newCommandOption($CommandOptionIds.`type`, "type of token ('s', 'p#' or 'n') ('#'=amount of parameters)", 't')
]

const NO_VALUE_SHORTS*: seq[char] =
    COMMAND_OPTIONS
    .filterIt(it.short.isSome() and not it.hasValue)
    .mapIt(it.short.get())

const NO_VALUE_LONGS*: seq[string] =
    COMMAND_OPTIONS
    .filterIt(not it.hasValue)
    .mapIt(it.longOptions())
    .foldl(a & b)

const HAS_VALUE_SHORTS*: seq[char] =
    COMMAND_OPTIONS
    .filterIt(it.short.isSome() and it.hasValue)
    .mapIt(it.short.get())

const HAS_VALUE_LONGS*: seq[string] =
    COMMAND_OPTIONS
    .filterIt(it.hasValue)
    .mapIt(it.longOptions())
    .foldl(a & b)

proc shortString*(id: CommandOptionIds): string =
    for commandOption in COMMAND_OPTIONS:
        if commandOption.name == $id:
            if commandOption.short.isSome():
                return "-" & $(commandOption.short.get())
            break
    return ""

proc parseCommandOptionIdByName*(idName: string): Option[CommandOptionIds] =
    for id in CommandOptionIds.items:
        let currentOption = COMMAND_OPTIONS.findFirst(o => o.name == $id).get()
        if $id == idName or idName in currentOption.alternatives:
            return some(id)
    return none(CommandOptionIds)

proc parseCommandOptionIdByShort*(short: string): Option[CommandOptionIds] =
    for commandOption in COMMAND_OPTIONS:
        if commandOption.short.isSome() and short == $(commandOption.short.get()):
            return parseCommandOptionIdByName(commandOption.name)
    return none(CommandOptionIds)

proc parseCommandOptionId*(idString: string): Option[CommandOptionIds] =
    let byShort = parseCommandOptionIdByShort(idString)
    if byShort.isSome():
        return byShort
    else:
        return parseCommandOptionIdByName(idString)

const PATH_COMMAND_OPTION_IDS*: seq[CommandOptionIds] =
    COMMAND_OPTIONS
        .filterIt(it.valueType in @[ValueType.filePath, ValueType.dirPath])
        .mapIt(it.name.parseCommandOptionIdByName)
        .flattened