import re, sequtils, strutils, algorithm
import ../services/echo_helper, ../data/constants

#[
    Parameterized token's parameter lists:
        - Is sorted
        - The token's value will contain numbers.
        - Loading token's value from sheet data json will also contain only numbers
]#

type
    LocaToken* = ref object of RootObj
        key*: string
        comments*: seq[string]
    SimpleLocaToken* = ref object of LocaToken
        value*: string
    ParameterizedLocaToken* = ref object of SimpleLocaToken
        parameterNames*: seq[string]
    NumericLocaToken* = ref object of LocaToken
        values*: seq[(string, string)]

let locaTokenDescriptions*: seq[(string, string)] = @[
    ("Simple token", "Contains a fixed string value."),
    ("Parameterized token", "Contains a string value with a fixed number of dynamic parameters. "),
    ("Numeric token", "Contains a list of variations of a string for a dynamic number."),
]

template areBothOfType[T](first, second: any): bool =
    first of T and second of T
        
proc matchesParameterized*(token: LocaToken, parameterRegex: Regex = parameterInValueRegex): bool =
    if token of ParameterizedLocaToken: return true
    if token of SimpleLocaToken:
        let simpleToken = SimpleLocaToken(token)
        if simpleToken.value.contains(parameterRegex):
            return true
    return false

proc toParameterized*(token: SimpleLocaToken, parameterRegex: Regex = parameterInValueRegex): LocaToken =
    let parameters = token.value
        .findAll(parameterRegex)
        .deduplicate()
        .sorted()
    var value = token.value
    for i, param in parameters.pairs:
        value = value.replace(re("\\{" & param & "\\}"), "{" & $i & "}")
    return ParameterizedLocaToken(key: token.key, comments: token.comments, value: value, parameterNames: parameters)

proc cleanDotDotNotation*(numericEntries: seq[(string, string)]): seq[(string, string)] =
    for index, pair in numericEntries.pairs:
        let key =
            if pair[0] != "..":             parseInt(pair[0]) #take
            elif index > 0:                 parseInt(numericEntries[index - 1][0]) + 1 #take last value +1
            elif numericEntries.len() > 1:  parseInt(numericEntries[1][0]) - 1 #take next value -1
            else:                           0 #is the only entry; number doesn't matter
        result.add(($key, pair[1]))

proc numericEntriesFormatted*(token: NumericLocaToken): seq[(string, string)] =
    var updatedEntries = newSeq[(string, string)]()
    for (key, value) in token.values:
        if key != "..":
            updatedEntries.add(($parseInt(key), value))
        else:
            updatedEntries.add((key, value))

    let maxKeyLength = updatedEntries.mapIt(it[0].len).max
    for i, (key, value) in updatedEntries.pairs:
        let offset = maxKeyLength - key.len()
        updatedEntries[i] = (key.indent(offset), value)
    
    return updatedEntries

proc renderRaw(self: ParameterizedLocaToken, withKey: bool): string =
    var value = self.value
    for i, param in self.parameterNames.pairs:
        value = value.replace(getParameterInStringRegex($i), param)
    if withKey:
        return self.key & ": \"" & value & "\""
    else:
        return "\"" & value & "\""

proc renderRaw(self: SimpleLocaToken, withKey: bool): string =
    if withKey:
        return self.key & ": \"" & self.value & "\""
    else:
        return "\"" & self.value & "\""

proc renderRaw(self: NumericLocaToken, withKey: bool): string =
    if withKey:
        result = self.key & ":"

    for (numKey, numValue) in self.numericEntriesFormatted():
        #TODO global rule for .. notation
        result.add(
            "\n\t" & numKey & ": \"" & numValue & "\""
        )

proc renderRaw(self: LocaToken, withKey: bool): string =
    if self of ParameterizedLocaToken: renderRaw(ParameterizedLocaToken(self), withKey)
    elif self of SimpleLocaToken: renderRaw(SimpleLocaToken(self), withKey)
    elif self of NumericLocaToken: renderRaw(NumericLocaToken(self), withKey)
    else: raiseException "'Render raw of loca token' found unknown token type."

proc renderRawFull*(self: LocaToken): string = self.renderRaw(withKey = true)

proc renderRawValueOnly*(self: LocaToken): string = self.renderRaw(withKey = false)

proc `$`*(token: LocaToken): string =
    if token.comments.len() > 0:
        let comment = token.comments.join("\n").split("\n").bullitPoint().join("\n")
        return comment & "\n" & token.renderRawFull()
    else:
        return token.renderRawFull()

proc typeEquals*(self, other: LocaToken): bool =
    areBothOfType[NumericLocaToken](self, other) or
    areBothOfType[ParameterizedLocaToken](self, other) or
    (
        areBothOfType[SimpleLocaToken](self, other) and
        not (self of ParameterizedLocaToken) and
        not (other of ParameterizedLocaToken)
    )

proc `==`*(self, other: LocaToken): bool =
    if self.key != other.key: return false

    if areBothOfType[SimpleLocaToken](self, other):
        return SimpleLocaToken(self).value == SimpleLocaToken(other).value
    elif areBothOfType[NumericLocaToken](self, other):
        return NumericLocaToken(self).values.cleanDotDotNotation() == NumericLocaToken(other).values.cleanDotDotNotation()
    
    return false

proc `!=`*(self, other: LocaToken): bool = not `==`(self, other)