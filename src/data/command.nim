import tables
import command_options

type 
    OnExecuteCommand = proc(options: Table[CommandOptionIds, string]): void

    Command* = object
        name*: string
        onExecute*: OnExecuteCommand
        requiredOptions*: seq[CommandOptionIds]
        descriptions*: seq[string]


proc newCommand*(
        name: string;
        onExecute: OnExecuteCommand,
        requiredOptions: openArray[CommandOptionIds],
        descriptions: openArray[string]
    ): Command = return Command(
        name: name,
        onExecute: onExecute,
        requiredOptions: @requiredOptions,
        descriptions: @descriptions
    )