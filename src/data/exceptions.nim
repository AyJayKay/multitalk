type
    ArgumentParseException* = object of Exception
    ValidationException* = object of Exception
    RuntimeException* = object of Exception