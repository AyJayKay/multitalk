import options, tables
import commands, command_options

type ParsedArguments* = object
        errors*: seq[string]
        hints*: seq[string]
        command*: Option[CommandIds]
        options*: Table[CommandOptionIds, string]