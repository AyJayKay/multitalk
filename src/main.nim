import options, tables, strformat
import services/[arg_parser, echo_helper, configuration_parser, configuration_helper]
import data/[command, commands, command_options, autonomous-options/helpOption, exceptions]

var arguments = parseCommandLineArguments()

for commandOptionId, _ in arguments.options:
    case commandOptionId
    of CommandOptionIds.help:
        showHelp()
        quit(0)
    of CommandOptionIds.version:
        echo "0.1.0"
        quit(0)
    of CommandOptionIds.quiet:
        muteEchoHelper()
    else: discard

try:
    let configArguments = arguments.findConfigurationPath().map(parseConfiguration)
    if configArguments.isSome():
        arguments.populateArgumentsByConfig(configArguments.get())
    setOptionsContext(arguments.options)

    echoArgumentParsingLogs(arguments)

    if arguments.command.isSome():
        for commandInstance in COMMANDS:
            if commandInstance.name == $(arguments.command.get()):
                commandInstance.onExecute(arguments.options)

except ArgumentParseException:
    let error = getCurrentExceptionMsg()
    if error.len() > 0:
        logError error
    let helpFlag = CommandOptionIds.help.shortString()
    logHint fmt"Use '{helpFlag}' for help."
    log "Aborting because of invalid arguments or configuration."
    quit(1)
except ValidationException:
    logError getCurrentExceptionMsg()
    log "Aborting because of validation errors."
    quit(1)
except:
    logRuntimeException()
    quit(1)