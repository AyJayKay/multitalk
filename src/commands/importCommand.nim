import tables, strformat, os
import ../services/[echo_helper, configuration_helper, sheet_parser, sheet_generator, validation_helper, loca_token_helper]
import ../data/[command_options, exceptions]

const descriptions* = [
    "Merges partial sheet(s) from translators into sub language sheets.",
    "Accepts specific language(s) option as filter."
]

proc execute*(options: Table[CommandOptionIds, string]) =
    ensureOptionValue(CommandOptionIds.sheet)
    ensureOptionValue(CommandOptionIds.sheetimport)
    let languages = getLanguageOptionValues(options)

    log "Parsing base language sheet..."
    let baseTokens = parseSheet(getSheetBaseLanguagePath())

    if languages.len() > 0:

        log fmt"Import path: {getImportSheetDirPath()}".indentAuto()
        indentLvlPlus()
        
        for language in languages:
            log language.indentAuto()
            indentLvlPlus()

            let path = getImportSheetPath(language)
            if fileExists(path):
                
                try:
                    log "Parsing sub language sheet...".indentAuto()
                    let targetTokens = parseSheet(getSheetPath(language)).validateUniqueTokenNames()

                    log "Parsing partial sheet (import path)...".indentAuto()
                    let newTokens = parseSheet(path).validateUniqueTokenNames()

                    log "Validating...".indentAuto()
                    let mergedTokens = targetTokens
                        .mergeAndReplaceWith(newTokens)
                        .validateCompareToBaseLanguageTokens(baseTokens)

                    log "Merging tokens...".indentAuto()
                    createSheet(language, getSheetPath(language), mergedTokens)
                except ValidationException:
                    logError getCurrentExceptionMsg().indentAuto()
                    log fmt"Aborting import for '{language}' because of validation errors.".indentAuto()
                except:
                    raise

            else:
                log "Nothing to import.".indentAuto()
            
            indentLvlMinus()
        indentLvlMinus()

    log "Done."
            