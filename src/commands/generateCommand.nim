import tables, strformat
import ../services/[sheet_parser, code_generator, sheet_data_generator, echo_helper, configuration_helper, json_helper, validation_helper]
import ../data/[command_options, exceptions]

const descriptions* = [
    "Generates production code.",
    "Fills the production data."
]

proc execute*(options: Table[CommandOptionIds, string]) =
    ensureOptionValue(CommandOptionIds.sheet)
    ensureOptionValue(CommandOptionIds.data)
    ensureOptionValue(CommandOptionIds.generated)

    log "Parsing base language sheet..."
    let baseTokens =
        parseSheet(getSheetBaseLanguagePath())
        .validateUniqueTokenNames()

    log "Generating C# code..."
    generateCSharp(baseTokens)
    
    log "Loadable data:"
    indentLvlPlus()
    log "Path: ".indentAuto() & getSheetDataDirPath()
    for language in getAllLanguages():
        log language.indentAuto()
        indentLvlPlus()

        try:
            let tokens = 
                if language == baseLanguage:
                    log "Base language is already parsed.".indentAuto()
                    baseTokens
                else:
                    log "Parsing sheet...".indentAuto()
                    parseSheet(getSheetPath(language))
                        .validateCompareToBaseLanguageTokens(baseTokens, filterIgnoredTokens = true)
                        .validateUniqueTokenNames()

            log "Generating data...".indentAuto()
            generateSheetData(tokens).writeJson(getSheetDataPath(language))

        except ValidationException:
            logError getCurrentExceptionMsg().indentAuto()
            log fmt"Aborting generation for '{language}' because of validation errors.".indentAuto()
        except:
            raise


        indentLvlMinus()
    indentLvlMinus()

    log "Done."