import tables, strformat, sequtils, sugar, options
import ../services/[echo_helper, sheet_parser, sheet_generator, sheet_diff_helper, configuration_helper, sheet_data_parser, functional_helper, loca_token_helper, validation_helper]
import ../data/[command_options, loca_token, constants, exceptions]

const descriptions* = [
    "Creates partial sheet(s) for translators.",
    "Accepts multiple langues to create multiple sheets."
]

proc filterAndPopulateUpdatedTokenComments*(currentTargetTokens: seq[LocaToken], baseTokenPairs: seq[LocaTokenPair]): seq[LocaToken] =
    for token in currentTargetTokens:
        let basePair = baseTokenPairs.findFirst(pair => pair.oldToken.key == token.key)
        if basePair.isSome():
            let oldBaseToken = basePair.get().oldToken
            let newBaseToken = basePair.get().newToken

            let baseToString = newBaseToken.renderRawValueOnly()

            var baseFromString = oldBaseToken.renderRawValueOnly()
            if oldBaseToken of ParameterizedLocaToken and token of ParameterizedLocaToken:
                let oldBaseParameterNames = ParameterizedLocaToken(oldBaseToken).parameterNames
                let oldSubParameterNames = ParameterizedLocaToken(token).parameterNames

                if oldBaseParameterNames.len() == oldSubParameterNames.len():
                    baseFromString = baseFromString.insertParameterNames(oldSubParameterNames)
                else:
                    baseFromString = baseFromString.insertSingleParameterName("?")
            
            result.add(
                token.populateComment(
                    $CommentConst.updatedToken,
                    "from " & baseFromString,
                    "to   " & baseToString
                )
            )

proc execute*(options: Table[CommandOptionIds, string]) =
    ensureOptionValue(CommandOptionIds.sheet)
    ensureOptionValue(CommandOptionIds.data)
    ensureOptionValue(CommandOptionIds.sheetexport)
    let languages = getLanguageOptionValues(options)

    if languages.len() > 0:

        log "Parsing base language..."
        let baseTokens =
            parseSheet(getSheetBaseLanguagePath())
            .validateUniqueTokenNames()
        log "Comparing to last generated data..."
        let updatedBaseTokenPairs = findUpdatedLocaTokens(baseTokens, parseSheetData(getSheetDataBaseLanguagePath()))
        
        let updatedBaseTokensCount = updatedBaseTokenPairs.len()
        if updatedBaseTokensCount > 0:
            indentLvlPlus()
            log fmt"Updated tokens: {updatedBaseTokensCount}".indentAuto()
            indentLvlMinus()

        log "Sheets for translators:"
        indentLvlPlus()
        log fmt"Path: {getExportSheetDirPath()}".indentAuto()
        for language in languages:
            log language.indentAuto()
            indentLvlPlus()

            if language == baseLanguage:
                log fmt"Base language sheet will not be exported.".indentAuto()
            else:
                try:
                    log "Parsing sheet...".indentAuto()
                    let targetTokens = parseSheet(getSheetPath(language))
                    log "Comparing...".indentAuto()

                    let addedTokens =
                        findAddedLocaKeys(baseTokens, targetTokens)
                        .mapIt(it.populateComment(
                            $CommentConst.newToken,
                            "original: " & it.renderRawValueOnly
                        ))
                    
                    let updatedTokens = targetTokens.filterAndPopulateUpdatedTokenComments(updatedBaseTokenPairs)

                    let addedTokensCount = addedTokens.len()
                    let updatedTokensCount = updatedTokens.len()
                    if addedTokensCount > 0 or updatedTokensCount > 0:
                        indentLvlPlus()
                        log fmt"New:     {addedTokensCount}".indentAuto()
                        log fmt"Updated: {updatedTokensCount}".indentAuto()
                        if updatedTokensCount != updatedBaseTokensCount:
                            logHint "Some tokens are updated in your base language sheet but not yet defined in this sub language sheet. (This, they will be treated as new tokens.)".indentAuto()
                        indentLvlMinus()

                        let outputPath = getExportSheetPath(language)
                        log "Creating sheet for translator...".indentAuto()
                        createSheet(language, outputPath, updatedTokens & addedTokens)

                    else:
                        log "Nothing changed.".indentAuto()
                except ValidationException:
                    logError getCurrentExceptionMsg().indentAuto()
                    log fmt"Aborting export for '{language}' because of validation errors.".indentAuto()
                except:
                    raise
            
            indentLvlMinus()
        indentLvlMinus()
    
    log "Done."
