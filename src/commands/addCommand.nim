import tables, strformat
import ../services/echo_helper, ../services/sheet_generator
import ../data/command_options

const descriptions* = [
    "Introduces a new key or language."
]

proc execute*(options: Table[CommandOptionIds, string]) =
    if options.hasKey(CommandOptionIds.token):
        let newKey = options[CommandOptionIds.token]
        log fmt"Adding key '{newKey}' to base language..."
        addLocaKey(newKey)
        log "Done."

    elif options.hasKey(CommandOptionIds.language):
        let newLanguage = options[CommandOptionIds.language]
        log fmt"Adding language '{newLanguage}'..."
        addLanguageFile(newLanguage)
        log "Done."

    else:
        raiseArgumentException "You need to add a key or a language!"