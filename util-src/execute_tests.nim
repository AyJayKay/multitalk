import os, osproc, terminal, sequtils, parseopt, sugar
from strutils import removeSuffix, indent, splitLines, split, startsWith, join, contains
import path_helper

let projectRootPath = getAppDir().parentDir
let mainTestFolderPath = projectRootPath / "test"
let tempOutputPath = mainTestFolderPath / "tmp"

const ignoredTestFolder = "test-utils"

proc getTestFilters(): seq[string] =
    var parser = commandLineParams().initOptParser()
    for _, key, value in parser.getopt():
        if key == "filter":
            for partialName in value.split(","):
                if partialName != "":
                    result.add(partialName)

let testFilters: seq[string] = getTestFilters()

proc containsAnyOf(full: string, filters: seq[string]): bool =
    return filters.len == 0 or filters.any(f => full.contains(f))

proc aggregateTests(testFolderPath: string, testFilters: seq[string]): seq[string] =
    if existsDir(testFolderPath): 
        for path in walkDirRec(testFolderPath, {PathComponent.pcFile}):
            let relPath = path.relativePath(mainTestFolderPath)
            if splitFile(path).ext == ".nim" and not relPath.startsWith(ignoredTestFolder) and relPath.containsAnyOf(testFilters):
                result.add(path)

proc buildRunTests(srcPaths: seq[string]): int =
    echo "collecting and compiling tests..."
    let testCodeFilePath = tempOutputPath / "exec_aggregated_tests.nim"

    var testCodeFile = open(testCodeFilePath, fmWrite)
    testCodeFile.writeLine("import " & ".." / ignoredTestFolder / "test_helper")
    for path in srcPaths:
        testCodeFile.writeLine("importTest \"" & path & "\"")
    testCodeFile.writeLine("aggregateOutputTestResults()")
    testCodeFile.close()

    assert 0 == execCmd(
        "nim c --hints=off --warnings:off -o:" & sanitizePathArg(tempOutputPath, isDir=true) & " " & sanitizePathArg(testCodeFilePath)
    )

    echo "execute tests:"
    let exitCode = execCmd(
        testCodeFilePath.changeFileExt("")
    )

    return exitCode

# main
if testFilters.len > 0:
    styledEcho fgYellow, styleBright, "Hint: Applied test filters:", resetStyle, " '", testFilters.join("' '"), "'"

if existsDir(tempOutputPath): removeDir(tempOutputPath)

let testSources = aggregateTests(mainTestFolderPath, testFilters)
createDir(tempOutputPath)
let failCount = buildRunTests(testSources)
if failCount == 0:
    removeDir(tempOutputPath)
quit(failCount)