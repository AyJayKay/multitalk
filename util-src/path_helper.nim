import os, strutils

proc sanitizePathArg*(path: string, isDir: bool = false): string =
    var sanitized = path

    let endsWithSeparator = sanitized.endsWith(os.DirSep)
    if isDir and not endsWithSeparator:
        sanitized &= os.DirSep
    elif not isDir and endsWithSeparator:
        sanitized = sanitized[0..^2]
    
    let escapeChar = when hostOS == "windows": "/" else: "\\"
    sanitized = sanitized.replace(" ", escapeChar & " ")
    
    return sanitized
