import sequtils, algorithm, ../test-utils/test_helper
import ../../src/data/command, ../../src/data/commands

test "commands must be unique":
    let commandNames = COMMANDS.mapIt(it.name)
    assert commandNames == commandNames.deduplicate, "There are commands with the same identifiers!"

test "command ids must match all commands":
    var commandNames = COMMANDS.mapIt(it.name)
    var commandIds = CommandIds.mapIt($it)
    
    commandNames.sort()
    commandIds.sort()

    assert commandNames == commandIds, "COMMANDS and CommandIds are not in sync!"