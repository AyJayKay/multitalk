import sequtils, algorithm, options, strformat, re, ../test-utils/test_helper
import ../../src/data/command_option, ../../src/data/command_options

test "command options must be unique":
    var options: seq[string]
    for option in COMMAND_OPTIONS:
        options &= option.allOptions()
    assert options == options.deduplicate, "There are command options with the same identifiers!"

test "command option ids must match all options":
    var optionNames = COMMAND_OPTIONS.mapIt(it.name)
    var optionIds = CommandOptionIds.mapIt($it)
    
    optionNames.sort()
    optionIds.sort()

    assert optionNames == optionIds, "COMMAND_OPTIONS and CommandOptionIds are not in sync!"

test "command option must have valid short":
    let allowedShortsRegex = re"[a-zA-Z0-9\?]"
    for option in COMMAND_OPTIONS:
        if option.short.isSome():
            assert match($(option.short.get()), allowedShortsRegex), fmt"CommandOption {$option.name} has an invalid short!"