proc dup(oldfd: FileHandle): FileHandle {.importc, header: "unistd.h".}
proc dup2(oldfd: FileHandle, newfd: FileHandle): cint {.importc, header: "unistd.h".}

let stdoutHandle = dup(stdout.getFileHandle())
let stderrHandle = dup(stderr.getFileHandle())
let stdinHandle = dup(stdin.getFileHandle())

proc assignFileHandle(io: File, fileHandle: FileHandle) =
    flushFile(io)
    discard dup2(fileHandle, io.getFileHandle())

proc setStdout*(file: File) = stdout.assignFileHandle(file.getFileHandle())
proc setStderr*(file: File) = stderr.assignFileHandle(file.getFileHandle())
proc setStdin*(file: File) = stdin.assignFileHandle(file.getFileHandle())

proc resetStdout*() = stdout.assignFileHandle(stdoutHandle)
proc resetStderr*() = stderr.assignFileHandle(stderrHandle)
proc resetStdin*() = stdin.assignFileHandle(stdinHandle)