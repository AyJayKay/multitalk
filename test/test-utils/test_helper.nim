import os, terminal, strutils, sequtils, std_io_helper, ../../src/services/echo_helper, re, solace_cat_helper
export os, terminal, strutils, sequtils, std_io_helper

const tempStdoutFileName = "stdout_tmp.txt"

### set up ###

let projectRootPath = getAppDir().parentDir

type FailedTest* = object
    filePath*: string
    caseName*: string
    output*: string

var currentTestFilePath: string
var currentTestCase: string
var failedTests = newSeq[FailedTest]()
var exceptionCaughtByWhen: ref Exception = nil

### tear down ###

proc resetWhen*() = exceptionCaughtByWhen = nil

proc cleanUp() =
    removeFile(tempStdoutFileName)
    resetWhen()
    currentTestCase = ""

### util ###

proc getCurrentTestCaseAsFileName*(): string =
    currentTestCase.strip()
    .replace(re"[^\w\d]", "_")

template importTest*(path: string) =
    styledEcho styleBright, path.relativePath(projectRootPath), ":"
    currentTestFilePath = path
    include path

proc styledException*(e: ref Exception) = styledEchoException(e)

proc dedent*(text: string): string =
    var texts = text.strip(true, true, {'\n'}).split("\n")
    while texts.allIt(it.startsWith(' ') or it.startsWith('\t') or it == ""):
        texts = texts.mapIt(if it.len > 0: it[1..<it.len] else: it)
        if (texts.allIt(it.len == 0)): break
    return texts.join("\n").strip(false, true)

template test*(name: string, body: untyped) =
    currentTestCase = name
    stdout.write (name & "...").indent(4)

    var caught = open(tempStdoutFileName, fmWrite)
    setStdout(caught)
    try:
        body

        resetStdout()
        caught.close()
        styledEcho fgGreen, "Passed"
    except:
        styledException getCurrentException()

        resetStdout()
        caught.close()
        let output = readFile(tempStdoutFileName)

        failedTests.add(FailedTest(
            filePath: currentTestFilePath,
            caseName: name,
            output: output
        ))
        styledEcho fgRed, "Failed"

    cleanUp()

proc echoSep() = styledEcho styleDim, "---+++---"

proc aggregateOutputTestResults*() =
    let failCount = failedTests.len
    if failCount > 0:
        for failedTest in failedTests:
            echoSep()
            let shortenedPath = failedTest.filePath.relativePath(projectRootPath)
            styledecho styleBright, shortenedPath, ":"
            styledecho "Case: ", fgRed, failedTest.caseName
            echo ""
            echo "Logs:"
            echo failedTest.output.replace(projectRootPath & os.DirSep, "")

        echoSep()
        let pluralS = if failCount > 1: "s" else: ""
        styledEcho styleBright, fgRed, $failCount, " test", pluralS, " failed!"
        echo getSolaceFromCat()

    else:
        echoSep()
        styledEcho styleBright, fgGreen, "All tests passed!"
        echo " - Nice day for pushin', ain't it? Hu Ha!\n"

    quit(failCount)

### helpers ###

template assertEquals*(value, expected: any) =
    let equality = value == expected
    assert equality, "\nexpected: " & $expected & "\n\nis found: " & $value

template assertContains*(sourceToCheck, expectedSubContent: any) =
    let isContaining: bool = expectedSubContent in sourceToCheck
    assert isContaining, "\nexpected: " & $expectedSubContent & "\n\nis found: " & $sourceToCheck

template When*(body: untyped) =
    try:
        body
    except:
        exceptionCaughtByWhen = getCurrentException()

proc assertRaisedException*(exceptionType: type) =
    assert exceptionCaughtByWhen != nil, "No exception was raised!"
    assert exceptionCaughtByWhen of ref exceptionType, "Exception of a different type than " & $exceptionType & " was raised!"

proc assertExceptionMessageContains*(partialMessage: string) =
    assert exceptionCaughtByWhen.msg.contains(partialMessage), "Actual msg: " & exceptionCaughtByWhen.msg