import random, strutils

const sentences = @[
    "Sorry for that! But here is a cute cat!",
    "Oh no, not that one again. Solace cat will help you!",
    "Ah no problem, that's gonna be an easy fix!",
    "Damn! Solace cat feels you!"
]

const cats = @[
 """
|\---/|
| o_o |
 \_^_/
""",
"""
 /\_/\
( o.o )
 > ^ <
""",
"""
  ／l 、
（ﾟ､ ｡ ７
　l   ~ヽ
　じし _, )ノ
"""
]

proc getSolaceFromCat*(): string =
    randomize()

    sentences[rand(0..<sentences.len)] &
    "\n" &
    cats[rand(0..<cats.len)].strip(true, true, {'\n'}) &
    "   meow"