import ../../src/data/loca_token

template simple*(k, v: string): LocaToken =
        LocaToken(SimpleLocaToken(key: k, value: v))

template parameterized*(k, v: string, p: varargs[string]): LocaToken =
        LocaToken(ParameterizedLocaToken(key: k, value: v, parameterNames: @p))

template numeric*(k: string, v: seq[(string, string)]): LocaToken =
        LocaToken(NumericLocaToken(key: k, values: v))