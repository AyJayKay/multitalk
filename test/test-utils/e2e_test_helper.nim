import os, osproc, strformat, options, nre, test_helper, ../../src/data/constants
from strutils import startsWith
export test_helper

var applicationPath = none(string)

type
    E2EEnvironment* = object
        path*: string
    ExecutionOutput* = object
        stdOut*: string
        exitCode*: int

proc createE2EEnvironment*(name: string = getCurrentTestCaseAsFileName()): E2EEnvironment =
    let testRootPath = getAppDir() / name
    if dirExists(testRootPath):
        raise newException(Exception, fmt"The e2e environment with name '{name}' already exists!")
    createDir(testRootPath)
    return E2EEnvironment(path: testRootPath)

proc createDir*(env: E2EEnvironment, relPath: string) =
    if relPath.isAbsolute:
        raise newException(Exception, fmt"The path to create a folder in an e2e environment needs to be relative!")
    createDir(env.path / relPath)
    assert dirExists(env.path / relPath)

proc createFile*(env: E2EEnvironment, relPath: string, content = "") =
    if relPath.isAbsolute:
        raise newException(Exception, fmt"The path to create a file in an e2e environment needs to be relative!")
    let(parent, fileName) = relPath.splitPath()
    if fileExists(env.path / relPath):
        raise newException(Exception, fmt"The file '{fileName}' already exists in the e2e environment!")
    if parent != "": env.createDir(parent)
    writeFile(env.path / parent / fileName, content.dedent())

proc createConfig*(env: E2EEnvironment, content: seq[(string, string)], name: string = defaultConfigName) =
    proc getLine(entry: (string, string)): string =
        if entry[1] == "": "-" & entry[0]
        else: entry[0] & " = " & entry[1]

    env.createFile(
        name,
        content.map(getLine).join("\n")
    )

proc ensureCompiledApplication() =
    if applicationPath.isNone() or not applicationPath.get().fileExists:
        echo "Compiling app for e2e test, as it doesn't exist..."
        let (buildOutput, buildCode) = execCmdEx "nim build"
        if buildCode != 0:
            echo buildOutput
            raise newException(Exception, "Building app for e2e tests failed!")
        let compiledPath = buildOutput.find(re"out: (.*) \[SuccessX\]").get().captures[0]
        assert compiledPath.len > 0
        assert compiledPath.isAbsolute()
        applicationPath = some(compiledPath)
    else:
        echo "Reusing app for e2e test, as it already exists..."

proc executeApp*(env: E2EEnvironment, arguments = "", relWorkingDir = "."): ExecutionOutput =
    ensureCompiledApplication()
    if relWorkingDir.isAbsolute() or relWorkingDir.startsWith(".."):
        raise newException(Exception, "The test execution from e2e test must stay in the environment! The working-dir has to be a relative path!")
    let (stdOut, exitCode) = execCmdEx fmt"cd {env.path / relWorkingDir} && {applicationPath.get()} {arguments}"
    return ExecutionOutput(stdOut: stdOut, exitCode: exitCode)

proc hasError*(run: ExecutionOutput): bool = run.exitCode != 0 or run.stdOut.contains "Error"

proc noError*(run: ExecutionOutput): bool =
    let hasError = run.hasError()
    if hasError:
        echo run.stdOut
    return not hasError