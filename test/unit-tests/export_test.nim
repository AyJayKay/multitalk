import ../test-utils/test_helper, ../test-utils/loca_token_tests_helper
import ../../src/[commands/exportCommand, services/sheet_diff_helper, data/loca_token]

test "simple token comment":
    #given
    let oldAndUpdatedBaseLangueTokens = @[LocaTokenPair(
                            oldToken: simple("myKey", "Hello you"),
                            newToken: simple("myKey", "Hello you!")
    )]
    let subLangueTokens = @[          simple("myKey", "Hallo du")]

    #when
    let commented = subLangueTokens.filterAndPopulateUpdatedTokenComments(oldAndUpdatedBaseLangueTokens)

    #then
    assertEquals commented.len(), 1
    assertEquals $commented[0], """
        # UPDATED
        # from "Hello you"
        # to   "Hello you!"
        myKey: "Hallo du"
    """.dedent()

test "numeric token comment":
    #given
    let oldAndUpdatedBaseLangueTokens = @[LocaTokenPair(
                            oldToken: numeric("myKey", @[("0","zero"), ("1","rest")]),
                            newToken: numeric("myKey", @[("1","one"),  ("..","rest")])
    )]
    let subLangueTokens = @[          numeric("myKey", @[("0","Null"), ("1","Rest")])]

    #when
    let commented = subLangueTokens.filterAndPopulateUpdatedTokenComments(oldAndUpdatedBaseLangueTokens)

    #then
    assertEquals commented.len(), 1
    assertEquals $commented[0], """
        # UPDATED
        # from 
        # 	0: "zero"
        # 	1: "rest"
        # to   
        # 	 1: "one"
        # 	..: "rest"
        myKey:
        	0: "Null"
        	1: "Rest"
    """.dedent()

test "parameterized token comment":
    #given
    let oldAndUpdatedBaseLangueTokens = @[LocaTokenPair(
                            oldToken: parameterized("myKey", "Hello {0} and {1}", "first", "second"),
                            newToken: parameterized("myKey", "Hello {1} and {0}!!!", "first", "second")
    )]
    let subLangueTokens = @[          parameterized("myKey", "Hallo {0} and {1}", "first", "second")]

    #when
    let commented = subLangueTokens.filterAndPopulateUpdatedTokenComments(oldAndUpdatedBaseLangueTokens)

    #then
    assertEquals commented.len(), 1
    assertEquals $commented[0], """
        # UPDATED
        # from "Hello {first} and {second}"
        # to   "Hello {second} and {first}!!!"
        myKey: "Hallo {first} and {second}"
    """.dedent()

test "parameterized token comment with name guessing from data":
    #given
    let oldAndUpdatedBaseLangueTokens = @[LocaTokenPair(
                            oldToken: parameterized("myKey", "Hello {0} and {1}", "0", "1"), #data didn't know names
                            newToken: parameterized("myKey", "Hello {0}, {1} and {2}", "first", "second", "third")
    )]
    let subLangueTokens = @[          parameterized("myKey", "Hallo {0} and {1}", "first", "second")]

    #when
    let commented = subLangueTokens.filterAndPopulateUpdatedTokenComments(oldAndUpdatedBaseLangueTokens)

    #then
    assertEquals commented.len(), 1
    assertEquals $commented[0], """
        # UPDATED
        # from "Hello {first} and {second}"
        # to   "Hello {first}, {second} and {third}"
        myKey: "Hallo {first} and {second}"
    """.dedent()

test "parameterized token comment with fall back for name guessing":
    #given
    let oldAndUpdatedBaseLangueTokens = @[LocaTokenPair(
                            oldToken: parameterized("myKey", "Hello {0} and {1}", "0", "1"), #data didn't know names
                            newToken: parameterized("myKey", "Hello {0}, {1} and {2}", "first", "second", "third")
    )]
    let subLangueTokens = @[          parameterized("myKey", "Hallo {0} and {1} {2}", "first", "second", "subLanguageDiff")]

    #when
    let commented = subLangueTokens.filterAndPopulateUpdatedTokenComments(oldAndUpdatedBaseLangueTokens)

    #then
    assertEquals commented.len(), 1
    assertEquals $commented[0], """
        # UPDATED
        # from "Hello {?} and {?}"
        # to   "Hello {first}, {second} and {third}"
        myKey: "Hallo {first} and {second} {subLanguageDiff}"
    """.dedent()