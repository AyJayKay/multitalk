import json
import ../../src/services/sheet_data_generator, ../../src/data/loca_token
import ../test-utils/test_helper, ../test-utils/loca_token_tests_helper

test "simple loca token is generated":
    #given
    let tokens = @[simple("myKey", "hello you!")]

    #when
    let json = generateSheetData(tokens)

    #then
    assertEquals json, %*{"myKey": "hello you!"}

test "parameterized loca token is generated":
    #given
    let tokens = @[parameterized("myKey", "hello {0}!", "name")]

    #when
    let json = generateSheetData(tokens)

    #then
    assertEquals json, %*{"myKey": "hello {0}!"}

test "numeric loca token is generated":
    #given
    let tokens = @[numeric("myKey", @[("0", "none"), ("1", "some"), ("10", "a lot")])]

    #when
    let json = generateSheetData(tokens)

    #then
    assertEquals json, %*{"myKey": {"0": "none", "1": "some", "10": "a lot"}}