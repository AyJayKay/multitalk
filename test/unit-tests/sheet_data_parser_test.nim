import streams
import ../test-utils/test_helper
import ../../src/services/sheet_data_parser, ../../src/data/loca_token

test "simple loca token is parsed":
    #given
    let stream = newStringStream("""{"myKey":"Hello you!"}""")

    #when
    let tokens = parseSheetData(stream)

    #then
    assertEquals tokens.len(), 1
    assert tokens[0] of SimpleLocaToken
    let token = SimpleLocaToken(tokens[0])
    assertEquals token.key, "myKey"
    assertEquals token.value, "Hello you!"

test "parameterized loca token is parsed":
    #given
    let stream = newStringStream("""{"myKey":"Hello {0}!"}""")

    #when
    let tokens = parseSheetData(stream)

    #then
    assertEquals tokens.len(), 1
    assert tokens[0] of ParameterizedLocaToken
    let token = ParameterizedLocaToken(tokens[0])
    assertEquals token.key, "myKey"
    assertEquals token.value, "Hello {0}!"
    assertEquals token.parameterNames, @["0"]

test "numeric loca token is parsed":
    #given
    let stream = newStringStream("""{"myKey":{
        "0":"none",
        "1":"some",
        "10":"a lot"
        }}""")

    #when
    let tokens = parseSheetData(stream)

    #then
    assertEquals tokens.len(), 1
    assert tokens[0] of NumericLocaToken
    let token = NumericLocaToken(tokens[0])
    assertEquals token.key, "myKey"
    let expectedValues = @[
        ("0","none"),
        ("1","some"),
        ("10","a lot")
    ]
    assertEquals token.values, expectedValues