import ../test-utils/test_helper, ../test-utils/loca_token_tests_helper
import ../../src/services/sheet_diff_helper, ../../src/data/loca_token

test "finding added simple loca token":
    #given
    let baseTokens = @[
        simple("myKey", "Hello you!"),
        simple("myOtherKey", "Hello two!"),
        simple("myEvenOtherKey", "Hello two!")
    ]
    let specificLangTokens = @[
        simple("myOtherKey", "Hello two!")
    ]

    #when
    let diff = findAddedLocaKeys(baseTokens, specificLangTokens)

    #then
    assertEquals diff.len(), 2
    assert diff[0] of SimpleLocaToken
    assert diff[1] of SimpleLocaToken
    let token1 = SimpleLocaToken(diff[0])
    assertEquals token1.key, "myKey"
    assertEquals token1.value, "Hello you!"
    let token2 = SimpleLocaToken(diff[1])
    assertEquals token2.key, "myEvenOtherKey"
    assertEquals token2.value, "Hello two!"

test "finding added numeric loca token":
    #given
    let baseTokens = @[
        numeric("myKey", @[("0", "none")]),
        numeric("myOtherKey", @[("0", "not none anymore")]),
        numeric("myEvenOtherKey", @[("0", "none"), ("1", "added entry")])
    ]
    let specificLangTokens = @[
        numeric("myKey", @[("0", "none")])
    ]

    #when
    let diff = findAddedLocaKeys(baseTokens, specificLangTokens)

    #then
    assertEquals diff.len(), 2
    assert diff[0] of NumericLocaToken
    assert diff[1] of NumericLocaToken
    let token1 = NumericLocaToken(diff[0])
    assertEquals token1.key, "myOtherKey"
    assertEquals token1.values, @[("0", "not none anymore")]
    let token2 = NumericLocaToken(diff[1])
    assertEquals token2.key, "myEvenOtherKey"
    assertEquals token2.values, @[("0", "none"), ("1", "added entry")]

test "find updated token":
    #given
    let oldTokens = @[
        simple("name", "Hero"),
        simple("item", "Book"),
        simple("tooltip", "Do good!")
    ]
    let updatedTokens = @[
        simple("name", "Hero"),
        simple("item", "Magic wand"),
        simple("tooltip", "Do good!")
    ]

    #when
    let diff = findUpdatedLocaTokens(updatedTokens, oldTokens)

    #then
    assertEquals diff.len(), 1
    let tokenPair = diff[0]
    let expectedOldToken = simple("item", "Book")
    let expectedNewToken = simple("item", "Magic wand")
    assert tokenPair.oldToken == expectedOldToken
    assert tokenPair.newToken == expectedNewToken
