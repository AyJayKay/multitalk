import streams, strutils, sequtils
import ../test-utils/test_helper
import ../../src/services/sheet_parser, ../../src/data/[loca_token, exceptions]

test "comments and empty lines are no tokens":
    #given
    let sheetStream = newStringStream("""
        #this should be ignored
                        #

        #this too
    """.dedent())

    #when
    let tokens = parseSheet(sheetStream)

    #then
    assertEquals tokens.len, 0

test "simple token should be parsed":
    #given
    let raw = "testKey: hello"
    let sheetStream = newStringStream(raw)

    #when
    let tokens = parseSheet(sheetStream)

    #then
    assertEquals tokens.len, 1
    assert tokens[0] of SimpleLocaToken
    let token = SimpleLocaToken(tokens[0])
    assertEquals token.key, "testKey"
    assertEquals token.value, "hello"
    assertEquals token.comments.len, 0

test "token may contain comments":
    #given
    let raw = """
        #this is a comment
        # leading space gets trimmed
            # this is indented
        testKey: hello
    """.dedent()
    let sheetStream = newStringStream(raw)

    #when
    let tokens = parseSheet(sheetStream)

    #then
    assertEquals tokens.len, 1
    assert tokens[0] of SimpleLocaToken
    let token = SimpleLocaToken(tokens[0])
    assertEquals token.key, "testKey"
    assertEquals token.value, "hello"
    assertEquals token.comments, @["this is a comment", "leading space gets trimmed", "this is indented"]

test "multiline token should be parsed":
    #given
    let sheetStream = newStringStream("""
        #starting directly after key
        testKey1: hello
            my
            friend

        #and starting in line after key
        testKey2:
            hello
            my
            friend
    """.dedent())

    #when
    let tokens = parseSheet(sheetStream)

    #then
    assertEquals tokens.len, 2
    assert tokens[0] of SimpleLocaToken
    assert tokens[1] of SimpleLocaToken
    let token1 = SimpleLocaToken(tokens[0])
    let token2 = SimpleLocaToken(tokens[1])
    assertEquals token1.key, "testKey1"
    assertEquals token2.key, "testKey2"
    assertEquals token1.value, "hello\nmy\nfriend"
    assertEquals token2.value, "hello\nmy\nfriend"
    assertEquals token1.comments, @["starting directly after key"]
    assertEquals token2.comments, @["and starting in line after key"]

test "multiline token may contain quote endings":
    #given
    let sheetStream = newStringStream("""
        testKey: "hello
            my "friend"
            !!!"
    """.dedent())

    #when
    let tokens = parseSheet(sheetStream)

    #then
    assertEquals tokens.len, 1
    assert tokens[0] of SimpleLocaToken
    let token = SimpleLocaToken(tokens[0])
    assertEquals token.key, "testKey"
    assertEquals token.value, "hello\nmy \"friend\"\n!!!"
    assertEquals token.comments.len, 0

test "multiline token without nestetd quoting may have no indentation":
    #given
    let sheetStream = newStringStream("""
        #starting directly after key
        testKey1: "hello
        my friend
        !!!"

        #starting in line after key
        testKey2:
        "hello
        my friend
        !!!"
    """.dedent())

    #when
    let tokens = parseSheet(sheetStream)

    #then
    #then
    assertEquals tokens.len, 2
    assert tokens[0] of SimpleLocaToken
    assert tokens[1] of SimpleLocaToken
    let token1 = SimpleLocaToken(tokens[0])
    let token2 = SimpleLocaToken(tokens[1])
    assertEquals token1.key, "testKey1"
    assertEquals token2.key, "testKey2"
    assertEquals token1.value, "hello\nmy friend\n!!!"
    assertEquals token2.value, "hello\nmy friend\n!!!"
    assertEquals token1.comments, @["starting directly after key"]
    assertEquals token2.comments, @["starting in line after key"]

test "multiline token with escaped quoting may have no indentation":
    #given
    let sheetStream = newStringStream("""
        testKey: "hello
        \"my friend\"
        !!!"
    """.dedent())

    #when
    let tokens = parseSheet(sheetStream)

    #then
    assertEquals tokens.len, 1
    assert tokens[0] of SimpleLocaToken
    let token = SimpleLocaToken(tokens[0])
    assertEquals token.key, "testKey"
    assertEquals token.value, "hello\n\"my friend\"\n!!!"
    assertEquals token.comments.len, 0

test "multiline token may contain escaped comment sign endings":
    #given
    let sheetStream = newStringStream("""
        testKey: "hello
            \#my friend
            !!!"
    """.dedent())

    #when
    let tokens = parseSheet(sheetStream)

    #then
    assertEquals tokens.len, 1
    assert tokens[0] of SimpleLocaToken
    let token = SimpleLocaToken(tokens[0])
    assertEquals token.key, "testKey"
    assertEquals token.value, "hello\n#my friend\n!!!"
    assertEquals token.comments.len, 0

test "escaped backslashes do not escape":
    #given
    let sheetStream = newStringStream("""
        testKey:
        "\\#
        \\"
    """.dedent())

    #when
    let tokens = parseSheet(sheetStream)

    #then
    assertEquals tokens.len, 1
    assert tokens[0] of SimpleLocaToken
    let token = SimpleLocaToken(tokens[0])
    assertEquals token.key, "testKey"
    assertEquals token.value, "\\#\n\\"
    assertEquals token.comments.len, 0

test "parameterized token should be parsed":
    #given
    let raw = "testKey: Hello {player}, welcome to {place}!"
    let sheetStream = newStringStream(raw)

    #when
    let tokens = parseSheet(sheetStream)

    #then
    assertEquals tokens.len, 1
    assert tokens[0] of ParameterizedLocaToken
    let token = ParameterizedLocaToken(tokens[0])
    assertEquals token.key, "testKey"
    assertEquals token.value, "Hello {1}, welcome to {0}!"
    assertEquals token.parameterNames, @["place", "player"]
    assertEquals token.comments.len, 0

test "parameterized token may reuse parameter names":
    #given
    let sheetStream = newStringStream("testKey: Hello {player}, your name is {player}, isn't it?")

    #when
    let tokens = parseSheet(sheetStream)

    #then
    assertEquals tokens.len, 1
    assert tokens[0] of ParameterizedLocaToken
    let token = ParameterizedLocaToken(tokens[0])
    assertEquals token.key, "testKey"
    assertEquals token.value, "Hello {0}, your name is {0}, isn't it?"
    assertEquals token.parameterNames, @["player"]
    assertEquals token.comments.len, 0

test "parameterized token is different if parameters get swapped":
    #given
    let sheetStream = newStringStream("""
        testKey1: Hello {time}, welcome to {space}!
        testKey2: Hello {space}, welcome to {time}!
    """.dedent())

    #when
    let tokens = parseSheet(sheetStream)

    #then
    assertEquals tokens.len, 2
    assert tokens[0] of ParameterizedLocaToken
    assert tokens[1] of ParameterizedLocaToken
    let token1 = ParameterizedLocaToken(tokens[0])
    let token2 = ParameterizedLocaToken(tokens[1])
    assert tokens[0] != tokens[1]
    assertEquals token1.value, "Hello {1}, welcome to {0}!"
    assertEquals token2.value, "Hello {0}, welcome to {1}!"
    assertEquals token1.comments.len, 0
    assertEquals token2.comments.len, 0

test "numeric token should be parsed with and without quotes":
    #given
    let raw = """apple:
    0: "no apples"
    1: 1 apple
    ..: {} apples"""
    let sheetStream = newStringStream(raw)

    #when
    let tokens = parseSheet(sheetStream)

    #then
    assertEquals tokens.len, 1
    assert tokens[0] of NumericLocaToken
    let token = NumericLocaToken(tokens[0])
    assertEquals token.key, "apple"
    let expectedValues = @[
        ("0", "no apples"),
        ("1", "1 apple"),
        ("..", "{} apples")
    ]
    assertEquals token.values, expectedValues
    assertEquals token.comments.len, 0

test "numeric token nested comments will be ignored":
    #given
    let raw = """
        # this is a fruit
        apple:
            0: "no apples"
            # it is okay that this line has no quotes
            1: 1 apple
            ..: {} apples
    """.dedent()
    let sheetStream = newStringStream(raw)

    #when
    let tokens = parseSheet(sheetStream)

    #then
    assertEquals tokens.len, 1
    assert tokens[0] of NumericLocaToken
    let token = NumericLocaToken(tokens[0])
    assertEquals token.key, "apple"
    let expectedValues = @[
        ("0", "no apples"),
        ("1", "1 apple"),
        ("..", "{} apples")
    ]
    assertEquals token.values, expectedValues
    assertEquals token.comments, @["this is a fruit"]

test "quotes should be escaped if they surround the texts":
    #given
    let sheetStream = newStringStream("""
        one: hello
        two: "hello"
        three: 'hello'
        four: `hello`
        five: ´hello´
        six: «hello»
        seven: ‹hello›
        eight: “hello”
        nine: ‘hello’
    """.dedent())

    #when
    let tokens = parseSheet(sheetStream)

    #then
    assertEquals tokens.len, 9
    for token in tokens:
        assert token of SimpleLocaToken
        assertEquals SimpleLocaToken(token).value, "hello"

test "escaping should work in all token types":
    #given
    let sheetStream = newStringStream("""
        simpleEscape: "Hello,
            these are curly braces: \{\}\n"
        paramEscape: "Hello {player},
            these are curly braces: \{\}\n"
        numericEscape:
            ..: "these are curly braces: \{\}\n"
    """.dedent())

    #when
    let tokens = parseSheet(sheetStream)

    #then
    assertEquals tokens.len, 3
    assert tokens[0] of SimpleLocaToken
    assert tokens[1] of ParameterizedLocaToken
    assert tokens[2] of NumericLocaToken
    let simpleToken = SimpleLocaToken(tokens[0])
    assertEquals simpleToken.value, "Hello,\nthese are curly braces: {}\n"
    let paramToken = ParameterizedLocaToken(tokens[1])
    assertEquals paramToken.value, "Hello {0},\nthese are curly braces: {}\n"
    assertEquals paramToken.parameterNames, @["player"]
    let numericToken = NumericLocaToken(tokens[2])
    assertEquals numericToken.values, @[("..", "these are curly braces: {}\n")]

test "token without value fails":
    #given
    let sheetStream = newStringStream("""
        myKey: "hello"

        myBrokenKey:
    """.dedent())

    #when
    When:
        discard parseSheet(sheetStream)

    #then
    assertRaisedException ValidationException
    assertExceptionMessageContains "'myBrokenKey'"
    assertExceptionMessageContains "no value"

test "full example sheet should be parsed":
    #given
    let sheetStream = newStringStream("""
        # this comment is ignored

        welcome: Welcome!

        # This is a parameterized text
        helloPlayer: Hello {name}!

        # This is a numeric variant text
        apple:
            0: no apples
            1: {} apple
            2: {} apples

        # For better readability use the range identifier and/or leading zeros
        quantity:
            ..: non
            01: some
            05: normal
            ..: a lot
        # This is a multiline text
        # Just do a normal line break. (Though \n is supported, too.)
        tutorialIntro: "We are so happy that you are here. This "awesome" text contains quotes and goes over multiple lines...
        See?\nIsn't that cool?"
    """.dedent())
    
    #when
    let tokens = parseSheet(sheetStream)

    #then
    assertEquals tokens.len, 5
    assert tokens[0] of SimpleLocaToken
    assert tokens[1] of ParameterizedLocaToken
    assert tokens[2] of NumericLocaToken
    assertEquals NumericLocaToken(tokens[2]).values.len, 3
    assert tokens[3] of NumericLocaToken
    assertEquals NumericLocaToken(tokens[3]).values.len,  4
    assert tokens[4] of SimpleLocaToken
    assert SimpleLocaToken(tokens[4]).value.endsWith("cool?")
    assertEquals tokens[0].comments.len, 0
    assertEquals tokens[1].comments.len, 1
    assertEquals tokens[2].comments.len, 1
    assertEquals tokens[3].comments.len, 1
    assertEquals tokens[4].comments.len, 2
    assert tokens.allIt(not it.comments.contains("this comment is ignored"))