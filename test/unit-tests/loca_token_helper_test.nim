import sequtils
import ../test-utils/test_helper, ../test-utils/loca_token_tests_helper
import ../../src/data/[loca_token, constants], ../../src/services/loca_token_helper

test "populate token with comments":
    #given
    let token = simple("myKey", "hello you")

    #when
    let commentedToken = token
        .populateComment("this", "is", "a")
        .populateComment("comment")

    #then
    assertEquals commentedToken.comments, @["this", "is", "a", "comment"]

test "to string hands out comments":
    #given
    let token = SimpleLocaToken(key: "myKey", value: "hello you", comments: @["this is", "a comment"])

    #when
    let text = $token

    #then
    assertEquals text, """# this is
# a comment
myKey: "hello you""""

test "remove tool comments":
    #given
    let tokens = @[
        simple("myKey1", "hello you").populateComment(
            "comment stays", $CommentConst.newToken, "comment goes"
        ),
        simple("myKey2", "hello you too").populateComment(
            "comment stays", $CommentConst.updatedToken, "comment goes"
        ),
        simple("myKey3", "hello you three").populateComment(
            "comment stays", "stays as well"
        )
    ]

    #when
    let cleanedTokens = tokens.map(removeToolGeneratedComments)

    #then
    assertEquals cleanedTokens.len(), 3
    assertEquals cleanedTokens[0].comments, @["comment stays"]
    assertEquals cleanedTokens[1].comments, @["comment stays"]
    assertEquals cleanedTokens[2].comments, @["comment stays", "stays as well"]


test "merging tokens":
    #given
    let simpleToken = simple("key1", "will stay")
    let oldParamToken = parameterized("key2", "will change {0}", "param")
    let numericToken = numeric("key3", @[("0","will stay")])
    let oldTokens = @[
        simpleToken,
        oldParamToken,
        numericToken
    ]

    let newParamToken = parameterized("key2", "has changed {0}", "lalala")
    let newSimpleToken = simple("key4", "is new")
    let newTokens = @[
        newParamToken,
        newSimpleToken
    ]

    #when
    let mergedTokens = oldTokens.mergeAndReplaceWith(newTokens)

    #then
    assertEquals mergedTokens.len(), 4
    assert mergedTokens.contains(simpleToken)
    assert not mergedTokens.contains(oldParamToken)
    assert mergedTokens.contains(numericToken)
    assert mergedTokens.contains(newParamToken)
    assert mergedTokens.contains(newSimpleToken)