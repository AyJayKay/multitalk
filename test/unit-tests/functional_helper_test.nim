import options, sugar
import ../test-utils/test_helper
import ../../src/services/functional_helper

test "first should return some of first element":
    assertEquals @[1].first(), some(1)
    assertEquals @[1,2,3,4,5].first(), some(1)
    assertEquals @[23,654,1,0,5432,4,124].first(), some(23)
    assertEquals @["a"].first(), some("a")
    assertEquals @["1","2","3","4"].first(), some("1")
    assertEquals @["m","9","Z","a"].first(), some("m")

test "first of empty should return none":
    assertEquals newSeq[string]().first(), none(string)

test "first is immutable":
    let mySeq = @[1,2,3,4,5]
    assertEquals mySeq.first(), some(1)

test "last should return some of last element":
    assertEquals @[1].last(), some(1)
    assertEquals @[1,2,3,4,5].last(), some(5)
    assertEquals @[23,654,1,0,5432,4,124].last(), some(124)
    assertEquals @["a"].last(), some("a")
    assertEquals @["1","2","3","4"].last(), some("4")
    assertEquals @["m","9","Z","a"].last(), some("a")

test "last of empty should return none":
    assertEquals newSeq[string]().last(), none(string)

test "last is immutable":
    let mySeq = @[1,2,3,4,5]
    assertEquals mySeq.last(), some(5)

test "front should return all except last":
    assertEquals @[1,2,3,4,5].front(), @[1,2,3,4]
    assertEquals @[23,654,1,0,5432,4,124].front(), @[23,654,1,0,5432,4]
    assertEquals @["1","2","3","4"].front(), @["1","2","3"]
    assertEquals @["m","9","Z","a"].front(), @["m","9","Z"]

test "front is immutable":
    let mySeq = @[1,2,3,4,5]
    assertEquals mySeq.front(), @[1,2,3,4]

test "front of empty and one element should return same":
    assertEquals newSeq[string]().front(), newSeq[string]()
    assertEquals @["a"].front(), @["a"]
    assertEquals @["a","b"].front(), @["a"]

test "tail should return all except first":
    assertEquals @[1,2,3,4,5].tail(), @[2,3,4,5]
    assertEquals @[23,654,1,0,5432,4,124].tail(), @[654,1,0,5432,4,124]
    assertEquals @["1","2","3","4"].tail(), @["2","3","4"]
    assertEquals @["m","9","Z","a"].tail(), @["9","Z","a"]

test "tail is immutable":
    let mySeq = @[1,2,3,4,5]
    assertEquals mySeq.tail(), @[2,3,4,5]

test "tail of empty and one element cases":
    assertEquals newSeq[string]().tail(), newSeq[string]()
    assertEquals @["a"].tail(), newSeq[string]()
    assertEquals @["a","b"].tail(), @["b"]


test "sequence to set":
    let mySetChar = {'a', 'b', 'c'}
    assertEquals @['a', 'b', 'c'].toSet(), mySetChar

test "find first some":
    #given
    let mySeq = @[(1, "a"), (0, "b"), (0, "c"), (2, "d")]
    proc getWithZero(x: (int, string)): bool = x[0] == 0

    #when
    let element = mySeq.findFirst(getWithZero)

    #then
    assertEquals element, some((0, "b"))

test "find first none":
    #given
    let mySeq = @[(1, "a"), (0, "b"), (0, "c"), (2, "d")]
    proc getWithThree(x: (int, string)): bool = x[0] == 3

    #when
    let element = mySeq.findFirst(getWithThree)

    #then
    assertEquals element, none((int, string))

test "flatten succeeds":
    #given
    let mySeq = @[some("Hallo"), none(string), some("you")]

    #when
    let flatSeq = mySeq.flattened

    #then
    assertEquals flatSeq, @["Hallo", "you"]