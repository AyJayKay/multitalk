import ../test-utils/test_helper
import ../../src/data/loca_token

test "all token equals succeeds":
    let simpleToken1 = SimpleLocaToken(key: "testKey", value: "hello")
    let simpleToken2 = SimpleLocaToken(key: "testKey", value: "hello")
    assert LocaToken(simpleToken1) == LocaToken(simpleToken2)
    assert not (LocaToken(simpleToken1) != LocaToken(simpleToken2))

    let paramToken1 = ParameterizedLocaToken(key: "testKey", value: "hello {0}", parameterNames: @["you"])
    let paramToken2 = ParameterizedLocaToken(key: "testKey", value: "hello {0}", parameterNames: @["you"])
    assert LocaToken(paramToken1) == LocaToken(paramToken2)
    assert not (LocaToken(paramToken1) != LocaToken(paramToken2))

    let numericToken1 = NumericLocaToken(key: "testKey", values: @[("0", "none"), ("1", "{} apple")])
    let numericToken2 = NumericLocaToken(key: "testKey", values: @[("0", "none"), ("1", "{} apple")])
    assert LocaToken(numericToken1) == LocaToken(numericToken2)
    assert not (LocaToken(numericToken1) != LocaToken(numericToken2))

test "all token equals fails":
    let token1 = SimpleLocaToken(key: "testKey", value: "hello")
    let token2 = SimpleLocaToken(key: "Key", value: "hello")
    assert not (LocaToken(token1) == LocaToken(token2))
    assert LocaToken(token1) != LocaToken(token2)

    let simpleToken1 = SimpleLocaToken(key: "testKey", value: "Hello")
    let simpleToken2 = SimpleLocaToken(key: "testKey", value: "hello")
    assert not (LocaToken(simpleToken1) == LocaToken(simpleToken2))
    assert LocaToken(simpleToken1) != LocaToken(simpleToken2)

    let paramToken1 = ParameterizedLocaToken(key: "testKey", value: "hello{0}", parameterNames: @["you"])
    let paramToken2 = ParameterizedLocaToken(key: "testKey", value: "hello {0}", parameterNames: @["you"])
    assert not (LocaToken(paramToken1) == LocaToken(paramToken2))
    assert LocaToken(paramToken1) != LocaToken(paramToken2)

    let numericToken1 = NumericLocaToken(key: "testKey", values: @[("0", "none"), ("1", "{} apples")])
    let numericToken2 = NumericLocaToken(key: "testKey", values: @[("0", "none"), ("1", "{} apple")])
    assert not (LocaToken(numericToken1) == LocaToken(numericToken2))
    assert LocaToken(numericToken1) != LocaToken(numericToken2)

test "numeric token clean dotdot notation works":
    #given
    var singleEntry = @[("..", "value")]
    var atNegEntry = @[("..", "value"), ("0", "value2")]
    var atZerosEntry = @[("..", "value"), ("1", "value2")]
    var atFirstEntry = @[("..", "value"), ("99", "value2")]
    var atLastEntry = @[("0", "value1"), ("..", "value2")]
    var betweenEntries = @[("1", "value"), ("..", "value2"), ("9", "value3"), ("..", "value4"), ("99", "value")]

    #when
    template runClean(entries: var seq[(string, string)]) =
        entries = entries.cleanDotDotNotation()

    runClean singleEntry
    runClean atNegEntry
    runClean atZerosEntry
    runClean atFirstEntry
    runClean atLastEntry
    runClean betweenEntries

    #then
    assertEquals singleEntry, @[("0", "value")]
    assertEquals atNegEntry, @[("-1", "value"), ("0", "value2")]
    assertEquals atZerosEntry, @[("0", "value"), ("1", "value2")]
    assertEquals atFirstEntry, @[("98", "value"), ("99", "value2")]
    assertEquals atLastEntry, @[("0", "value1"), ("1", "value2")]
    assertEquals betweenEntries, @[("1", "value"), ("2", "value2"), ("9", "value3"), ("10", "value4"), ("99", "value")]

test "render raw of parameterized token":
    #given
    let token = LocaToken(ParameterizedLocaToken(
        key: "myKey",
        value: "Hello {0} and {1}!",
        parameterNames: @["name1", "name2"]
    ))

    #when
    let fullRaw = token.renderRawFull()
    let valueRaw = token.renderRawValueOnly()

    #then
    assertEquals fullRaw, """myKey: "Hello {name1} and {name2}!""""
    assertEquals valueRaw, "\"Hello {name1} and {name2}!\""

test "render raw of simple token":
    #given
    let token = LocaToken(SimpleLocaToken(
        key: "myKey",
        value: "Hello you!"
    ))

    #when
    let fullRaw = token.renderRawFull()
    let valueRaw = token.renderRawValueOnly()

    #then
    assertEquals fullRaw, """myKey: "Hello you!""""
    assertEquals valueRaw, "\"Hello you!\""

test "render raw of numeric token":
    #given
    let token = LocaToken(NumericLocaToken(
        key: "myKey",
        values: @[
            ("0", "none"),
            ("2", "more"),
            ("100", "a lot")
        ]
    ))

    #when
    let fullRaw = token.renderRawFull()
    let valueRaw = token.renderRawValueOnly()

    #then
    assertEquals fullRaw, """myKey:
	  0: "none"
	  2: "more"
	100: "a lot""""
    assertEquals valueRaw, "\n\t  0: \"none\"\n\t  2: \"more\"\n\t100: \"a lot\""

test "numeric entries with multiple zeros do not break":
    #given
    let token = NumericLocaToken(key: "myKey", values: @[
        ("00", "zero"),
        ("001", "one"),
        ("0002", "two")
    ])

    #when
    let entries = token.numericEntriesFormatted()

    #than
    assertEquals entries.len, 3
    assertEquals entries[0][0], "0"
    assertEquals entries[1][0], "1"
    assertEquals entries[2][0], "2"