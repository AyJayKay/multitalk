import strformat
import ../test-utils/test_helper, ../test-utils/loca_token_tests_helper
import ../../src/services/[validation_helper, echo_helper], ../../src/data/[loca_token, exceptions]

test "key name is valid":
    #given
    let keys = @[
        "playerName",
        "playername",
        "PlayerName",
        "player-name",
        "player-name-",
        "player_Name",
        "player_Name_",
        "playerName123",
        "_123",
        "_123abc"
    ]
    
    #when
    for key in keys:
        validateLocaTokenKeyName(key)
    
    #then
    #no validation exception is raised

test "key name is invalid":
    #given
    let keys = @[
        "playerName!",
        "+playerName"
    ]
    
    #when
    for key in keys:
        When:
            validateLocaTokenKeyName(key)
    
        #then
        assertRaisedException ValidationException
        assertExceptionMessageContains fmt"'{key}' contains invalid character"
        resetWhen()


test "key must not use reserved name":
    #given
    let key = "DynamicGet"
    
    #when
    When:
        validateLocaTokenKeyName(key)
    
    #then
    assertRaisedException ValidationException
    assertExceptionMessageContains "is reserved"

test "key name must not start with number":
    #given
    let key = "2levelTitle"
    
    #when
    When:
        validateLocaTokenKeyName(key)
    
    #then
    assertRaisedException ValidationException
    assertExceptionMessageContains "may not start with a didget"

test "only number keys may start with underscores":
    #given
    let key = "_abc"
    
    #when
    When:
        validateLocaTokenKeyName(key)
    
    #then
    assertRaisedException ValidationException
    assertExceptionMessageContains "may not start with '_'"

test "key name is free success":
    #given
    let key = "myKeyFour"
    let tokens = @[
        simple("myKey", "hello"),
        simple("myKeyTwo", "hello you"),
        simple("myKeyThree", "hello you too")
    ]
    
    #when
    key.validateFreeLocaTokenKeyNameIn(tokens)
    
    #then
    #no validation exception is raised

test "key name is not free raises exception":
    #given
    let key = "myKeyTwo"
    let tokens = @[
        simple("myKey", "hello"),
        simple("myKeyTwo", "hello you"),
        simple("myKeyThree", "hello you too")
    ]
    
    #when
    When:
        key.validateFreeLocaTokenKeyNameIn(tokens)
    
    #then
    assertRaisedException ValidationException
    assertExceptionMessageContains "already exists"

test "numeric token validation success":
    #given
    let tokens = @[
        numeric("myKey1", @[ ("0", "low"),  ("1","mid"),  ("2","high")]),
        numeric("myKey2", @[("..", "low"),  ("1","mid"),  ("2","high")]),
        numeric("myKey3", @[ ("0", "low"), ("..","mid"),  ("2","high")]),
        numeric("myKey4", @[ ("0", "low"),  ("1","mid"), ("..","high")]),
        numeric("myKey5", @[ ("-999", "very low"),  ("..","low"),  ("0","mid"), ("..","high"), ("999","very high")])
    ]

    #when
    for token in tokens:
        validateNumericLocaToken(NumericLocaToken(token))
   
    #then
    #no validation exception is raised

test "numeric token must have valid dotdot notation":
    #given
    let tokens = @[
        numeric("myKey1", @[("0", "low"), ("..","mid"), ("..","high")]),
        numeric("myKey2", @[("..", "low"), ("..","mid"), ("10","high")]),
        numeric("myKey3", @[("0", "low"), ("..", "low"), ("..","mid"), ("10","high")])
    ]

    for token in tokens:
        #when
        When:
            validateNumericLocaToken(NumericLocaToken(token))
        
        #then
        assertRaisedException ValidationException
        assertExceptionMessageContains "multiple '..'-notation"
        resetWhen()

test "numeric token must have valid order":
    #given
    let tokens = @[
        numeric("myKey1", @[("0", "low"), ("2","mid"), ("1","high")]),
        numeric("myKey2", @[("0", "low"), ("1","mid"), ("1","high")])
    ]

    #when
    for token in tokens:
        When:
            validateNumericLocaToken(NumericLocaToken(token))
        
        #then
        assertRaisedException ValidationException
        assertExceptionMessageContains "unsorted or non-unique"
        resetWhen()

test "sheet comparison of token type success":
    #given
    let baseTokens = @[
        simple("myKey1", "Hello"),
        parameterized("myKey2", "Hello {0}", "name"),
        numeric("myKey3", @[("0", "none"), ("1","some")])
    ]
    let germanTokens = @[
        simple("myKey1", "Hallo"),
        parameterized("myKey2", "Hallo {0}", "name"),
        numeric("myKey3", @[("0", "keine"), ("1","einige")])
    ]

    #when
    let result = germanTokens.validateCompareToBaseLanguageTokens(baseTokens, filterIgnoredTokens = true)

    #then
    #no validation exception is raised
    assertEquals result, germanTokens

test "sheet comparison fails for token type":
    #given
    let baseTokensAndTargetTokens = @[
        (
            @[simple("myKey1", "Hello")],
            @[parameterized("myKey1", "Hallo {0}", "name")]
        ),
        (
            @[simple("myKey1", "Hello")],
            @[numeric("myKey1", @[("0", "keine"), ("1","einige")])]
        ),
        (
            @[parameterized("myKey1", "Hello {0}", "name")],
            @[numeric("myKey1", @[("0", "keine"), ("1","einige")])]
        )
    ]

    #when
    for pair in baseTokensAndTargetTokens:
        When:
            discard pair[0].validateCompareToBaseLanguageTokens(pair[1])
        
        #then
        assertRaisedException ValidationException
        assertExceptionMessageContains "token definition of this sheet is invalid"
        resetWhen()

test "sheet comparison succeeds for parameterized tokens":
    #given
    let baseTokensAndTargetTokens = @[
        (
            @[parameterized("myKey1", "Hello {0}", "name")],
            @[parameterized("myKey1", "Hallo {0}", "name")]
        ),
        (
            @[parameterized("myKey1", "Hello {0}, {1}", "name",  "title")],
            @[parameterized("myKey1", "Hallo {1}, {0}", "title", "name")]
        ),
        (
            @[parameterized("myKey1", "Hello {0}, {1}",           "name", "title")],
            @[ parameterized("myKey1", "Hallo {0}, {1}, ja {0}!", "name", "title")]
        ),
        (
            @[parameterized("myKey1", "Hello {0}, {1}", "name",  "title")],
            @[parameterized("myKey1", "Hallo {1}, {0}", "title", "name")]
        )
    ]

    #when
    for pair in baseTokensAndTargetTokens:
        When:
            discard pair[0].validateCompareToBaseLanguageTokens(pair[1])
        
        #then
        #no exception should be raised

test "sheet comparison fails for parameter missmatch":
    #given
    let baseTokensAndTargetTokens = @[
        (
            @[parameterized("myKey1", "Hello {0}",      "name")],
            @[parameterized("myKey1", "Hallo {0}, {1}", "name", "additional")]
        ),
        (
            @[parameterized("myKey1", "Hello {0}, {1}", "name", "additional")],
            @[parameterized("myKey1", "Hallo {0}",      "name")]
        ),
        (
            @[parameterized("myKey1", "Hello {0}, {1}", "name", "title")],
            @[parameterized("myKey1", "Hallo {0}, {1}", "name", "additional")]
        )
    ]

    #when
    for pair in baseTokensAndTargetTokens:
        When:
            discard pair[0].validateCompareToBaseLanguageTokens(pair[1])
        
        #then
        assertRaisedException ValidationException
        assertExceptionMessageContains "token definition of this sheet is invalid"
        resetWhen()

test "validate token name uniqueness fails":
    #given
    let tokens = @[
        simple("myKey1", "hello you"),
        parameterized("anotherKey", "la la la {0}", "tone"),
        simple("myKey1", "again")
    ]

    #when
    When:
        discard tokens.validateUniqueTokenNames()
    
    #then
    assertRaisedException ValidationException
    assertExceptionMessageContains "defined multiple times"