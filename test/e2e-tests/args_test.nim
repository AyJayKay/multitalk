import ../test-utils/e2e_test_helper
import os, strformat, ../../src/data/constants

test "shorts, options and different value concatenation is supported":
    # given
    let env = createE2EEnvironment()
    env.createFile("sheets" / "dwarven.txt")
    env.createFile("sheets-import" / "dwarven.txt", "tokenName: Hello you!")
    let importPath = env.path / "sheets-import"

    env.createDir("data")
    env.createDir("code")

    env.createConfig(@[
        ("s", "sheets"),
        ("import", "sheets-import")
    ])
    let configPath = env.path / defaultConfigName

    # when
    let run = env.executeApp("import -ddata -g:code --languages dwarven")

    # then
    assert fmt"Using config: {configPath}" in run.stdOut
    assert fmt"Import path: {importPath}" in run.stdOut
    assert "Merging tokens" in run.stdOut
    assert "tokenName: \"Hello you!\"" in readFile(env.path / "sheets" / "dwarven.txt")
    assert run.noError

test "unsupported arguments raise errors":
    # given
    let env = createE2EEnvironment()

    # when
    let runLong = env.executeApp("generate --unknown")
    let runShort = env.executeApp("generate -u:bla")

    # then
    assert "'unknown' is not a valid option" in runLong.stdOut
    assert "'u' is not a valid option" in runShort.stdOut
    assert "Generating" notin runLong.stdOut
    assert "Generating" notin runShort.stdOut
    assert runLong.hasError
    assert runShort.hasError

test "unsupported command raises error":
    # given
    let env = createE2EEnvironment()

    # when
    let run = env.executeApp("unknown")

    # then
    assert "'unknown' is not a valid command" in run.stdOut
    assert run.hasError