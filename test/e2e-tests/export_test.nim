import ../test-utils/e2e_test_helper
import os, ../../src/data/command_options

test "export error does not block other sub languages":
    # given
    let env = createE2EEnvironment()
    env.createConfig(@[
        ($sheet, "sheets"),
        ($sheetexport, "sheet-export"),
        ($data, "json")
    ])
    env.createDir("sheets")
    env.createDir("sheet-export")
    env.createDir("json")
    env.createFile("sheets" / "english.txt", "myToken: \"Hello you!\"")
    env.createFile("sheets" / "dwarven.txt")
    env.createFile("sheets" / "bogonian.txt", "+-not supported syntax-+")
    env.createFile("sheets" / "elven.txt")

    # when
    let run = env.executeApp("export")

    # then
    assert "dwarven" in run.stdOut
    assert "bogonian" in run.stdOut
    assert "elven" in run.stdOut
    assert "'+-not supported syntax-+' is not a valid loca definition" in run.stdOut
    assert "Done." in run.stdOut
    assert run.hasError
    assertContains readFile(env.path / "sheet-export" / "dwarven.txt"), "myToken"
    assertContains readFile(env.path / "sheet-export" / "elven.txt"), "myToken"

test "updated base language key is treated as new key for sub language":
    # given
    let env = createE2EEnvironment()
    env.createConfig(@[
        ($sheet, "sheets"),
        ($sheetexport, "sheet-export"),
        ($data, "json"),
        ($generated, "code")
    ])
    env.createDir("sheets")
    env.createDir("sheet-export")
    env.createDir("code")
    env.createDir("json")
    env.createFile("sheets" / "english.txt", "myToken: \"Hello you!\"")
    env.createFile("sheets" / "dwarven.txt")

    # when
    let firstRun = env.executeApp("generate")
    writeFile(env.path / "sheets" / "english.txt", "myToken: \"Updated!\"")
    let secondRun = env.executeApp("export")

    # then
    assert firstRun.noError
    assert "Some tokens are updated in your base language sheet but not yet defined in this sub language sheet" in secondRun.stdOut
    assertContains readFile(env.path / "sheet-export" / "dwarven.txt"), "Updated!"
    assert secondRun.noError