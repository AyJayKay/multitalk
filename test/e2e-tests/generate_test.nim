import ../test-utils/e2e_test_helper
import os, ../../src/data/command_options

test "generate error does not block other sub languages":
    # given
    let env = createE2EEnvironment()
    env.createConfig(@[
        ($sheet, "sheets"),
        ($data, "json"),
        ($generated, "code")
    ])
    env.createDir("sheets")
    env.createDir("json")
    env.createDir("code")
    env.createFile("sheets" / "english.txt", "myToken: \"Hello you!\"")
    env.createFile("sheets" / "dwarven.txt", "myToken: \"Tada!\"")
    env.createFile("sheets" / "bogonian.txt", "+-not supported syntax-+")
    env.createFile("sheets" / "elven.txt", "myToken: \"Lyrtkt!\"")

    # when
    let run = env.executeApp("generate")

    # then
    assert "dwarven" in run.stdOut
    assert "bogonian" in run.stdOut
    assert "elven" in run.stdOut
    assert "'+-not supported syntax-+' is not a valid loca definition" in run.stdOut
    assert "Done." in run.stdOut
    assert run.hasError
    assertContains readFile(env.path / "json" / "dwarven.json"), "myToken"
    assertContains readFile(env.path / "json" / "elven.json"), "myToken"

test "sub tokens, that are not in base, are ignored on generate":
    # given
    let env = createE2EEnvironment()
    env.createConfig(@[
        ($sheet, "sheets"),
        ($data, "json"),
        ($generated, "code")
    ])
    env.createDir("sheets")
    env.createDir("json")
    env.createDir("code")
    env.createFile("sheets" / "english.txt", "myToken: \"Hello you!\"")
    env.createFile("sheets" / "dwarven.txt", """
        myToken: "Tada!"
        mySubSpecificToken: "invisible"
    """)

    # when
    let run = env.executeApp("generate")

    # then
    assert run.noError
    assertContains run.stdOut, "Token 'mySubSpecificToken' is not listed in the base language sheet. It will be ignored!"
    assert "mySubSpecificToken" notin readFile(env.path / "json" / "dwarven.json")