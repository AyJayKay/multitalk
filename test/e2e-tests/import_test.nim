import ../test-utils/e2e_test_helper
import os, ../../src/data/command_options

test "imported partial sheets are not cleaned up on import":
    # given
    let env = createE2EEnvironment()
    env.createConfig(@[
        ($sheet, "sheets"),
        ($sheetimport, "sheet-import")
    ])
    env.createDir("sheets")
    env.createDir("sheet-import")
    env.createFile("sheets" / "english.txt")
    env.createFile("sheet-import" / "english.txt", """
        # [english]

        # a new token
        myToken: "Hello you!"
    """)

    # when
    let run = env.executeApp("import")

    # then
    assert fileExists(env.path / "sheet-import" / "english.txt")
    assertContains readFile(env.path / "sheet-import" / "english.txt"), "myToken: \"Hello you!\""
    assert run.noError

test "import error does not block other sub languages":
    # given
    let env = createE2EEnvironment()
    env.createConfig(@[
        ($sheet, "sheets"),
        ($sheetimport, "sheet-import")
    ])
    env.createDir("sheets")
    env.createDir("sheet-import")
    env.createFile("sheets" / "english.txt", "myToken: \"Hello you!\"")
    env.createFile("sheets" / "dwarven.txt")
    env.createFile("sheets" / "bogonian.txt")
    env.createFile("sheets" / "elven.txt")

    env.createFile("sheet-import" / "dwarven.txt", "myToken: \"Tada!\"")
    env.createFile("sheet-import" / "bogonian.txt", "+-not supported syntax-+")
    env.createFile("sheet-import" / "elven.txt", "myToken: \"Lydrktk!\"")

    # when
    let run = env.executeApp("import")

    # then
    assert "dwarven" in run.stdOut
    assert "bogonian" in run.stdOut
    assert "elven" in run.stdOut
    assert "'+-not supported syntax-+' is not a valid loca definition" in run.stdOut
    assert "Done." in run.stdOut
    assert run.hasError
    assertContains readFile(env.path / "sheets" / "dwarven.txt"), "Tada!"
    assertContains readFile(env.path / "sheets" / "elven.txt"), "Lydrktk!"