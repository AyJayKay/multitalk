import ../test-utils/e2e_test_helper
import os, strformat, ../../src/data/[constants, command_options]

test "default config name is searched upwards in workdir":
    # given
    let env = createE2EEnvironment()
    env.createFile(defaultConfigName)
    let configAtRootPath = env.path / defaultConfigName
    env.createFile("my-awesome-project" / "inside" / defaultConfigName)
    let configAtInsideFolderPath = env.path / "my-awesome-project" / "inside" / defaultConfigName

    # when
    let runFromProjectFolder = env.executeApp(
        relWorkingDir = "my-awesome-project"
    )
    let runFromInsideFolder = env.executeApp(
        relWorkingDir = "my-awesome-project" / "inside"
    )

    # then
    let usedConfigAtRootMessage = fmt"Using config: {configAtRootPath}"
    let usedConfigAtInsideFolderMessage = fmt"Using config: {configAtInsideFolderPath}"

    assert usedConfigAtRootMessage in runFromProjectFolder.stdOut
    assert usedConfigAtInsideFolderMessage notin runFromProjectFolder.stdOut

    assert usedConfigAtInsideFolderMessage in runFromInsideFolder.stdOut
    assert usedConfigAtRootMessage notin runFromInsideFolder.stdOut

test "config can be specified absolute or relative to workdir":
    # given
    let env = createE2EEnvironment()
    env.createFile(defaultConfigName)
    let configAtRootPath = env.path / defaultConfigName
    env.createFile("my-configs" / "my-lang-config.txt")
    let configAtConfigsFolderPath = env.path / "my-configs" / "my-lang-config.txt"

    # when
    let runFullPath = env.executeApp(fmt"--config '{configAtConfigsFolderPath}'")
    let runRelPath = env.executeApp(fmt"--config 'my-configs{os.DirSep}my-lang-config.txt'")

    # then
    let usedConfigAtConfigsFolderMessage = fmt"Using config: {configAtConfigsFolderPath}"
    let usedDefaultConfigMessage = fmt"Using config: {configAtRootPath}"

    assert usedConfigAtConfigsFolderMessage in runFullPath.stdOut
    assert usedConfigAtConfigsFolderMessage in runRelPath.stdOut

    assert usedDefaultConfigMessage notin runFullPath.stdOut
    assert usedDefaultConfigMessage notin runRelPath.stdOut

test "paths in arguments are relative to workdir":
    # given
    let env = createE2EEnvironment()
    env.createFile("sheets" / "english.txt")
    env.createDir("sheets-import")
    let importPath = env.path / "sheets-import"

    env.createDir("inside")

    # when
    let run = env.executeApp(
        arguments = fmt"import --sheet=..{os.DirSep}sheets --sheetimport=..{os.DirSep}sheets-import",
        relWorkingDir = "inside"
    )

    # then
    assert "sing config" notin run.stdOut
    assert fmt"Import path: {importPath}" in run.stdOut
    assert "Nothing to import" in run.stdOut
    assert run.noError

test "paths in config are relative to config file":
    # given
    let env = createE2EEnvironment()
    env.createFile("sheets" / "english.txt")
    env.createDir("project" / "sheets-import")
    let importPath = env.path / "project" / "sheets-import"

    env.createConfig(@[
            ($sheet, "../sheets"),
            ($sheetimport, "sheets-import")
        ],
        "project" / defaultConfigName
    )
    let configPath = env.path / "project" / defaultConfigName

    # when
    let run = env.executeApp(
        arguments = "import",
        relWorkingDir = "project"
    )

    # then
    assert fmt"Using config: {configPath}" in run.stdOut
    assert fmt"Import path: {importPath}" in run.stdOut
    assert "Nothing to import" in run.stdOut
    assert run.noError

test "arguments overwrite config values":
    # given
    let env = createE2EEnvironment()
    env.createFile("sheets" / "english.txt")
    env.createDir("sheets-import")
    let importPath = env.path / "sheets-import"

    env.createConfig(@[
        ($sheet, "sheets-not-to-use"),
        ($sheetimport, "sheets-import-not-to-use")
    ])
    let configPath = env.path / defaultConfigName
    env.createFile("sheets-not-to-use" / "english.txt")
    env.createFile("sheets-import-not-to-use" / "english.txt", "tokenNotToUse: Hello you!")

    # when
    let run = env.executeApp("import --sheet=sheets --sheetimport=sheets-import")

    # then
    assert fmt"Using config: {configPath}" in run.stdOut #using config
    assert fmt"Import path: {importPath}" in run.stdOut  #but not the paths from config
    assert "Nothing to import" in run.stdOut
    assert "Hello" notin readFile(env.path / "sheets-not-to-use" / "english.txt")
    assert run.noError

test "missing config raises error":
    #given
    let env = createE2EEnvironment()
    let missingConfigPath = env.path / "missing-config.conf"

    #ensure
    assert not existsFile(missingConfigPath)

    #when
    let run = env.executeApp("import -c=missing-config.conf")

    #then
    assert fmt"Couldn't find config at {missingConfigPath}" in run.stdOut
    assert run.hasError

test "unsupported config options raise errors":
    # given
    let env = createE2EEnvironment()
    env.createFile("sheets" / "dwarven.txt")
    env.createFile("sheets-import" / "dwarven.txt", "tokenName: Hello you!")
    let importPath = env.path / "sheets-import"
    
    env.createConfig(@[
        ("sheets", "sheets"),        # fine
        ("import", "sheets-import"), # fine
        ("unknown", "bla")           # breaks
    ])

    # when
    let run = env.executeApp("import")

    # then
    assert "'unknown' is not a valid config option" in run.stdOut
    assert importPath notin run.stdOut
    assert run.hasError